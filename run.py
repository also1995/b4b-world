import uvicorn
from alembic.config import Config
from alembic import command
from app.config import config

if __name__ == '__main__':
    # run migrations
    # alembic_cfg = Config()
    # alembic_cfg.set_main_option('script_location', "migrations")
    # alembic_cfg.set_main_option('sqlalchemy.url', config.DB_URL)
    # command.upgrade(alembic_cfg, 'head')

    from app import app
    uvicorn.run(app, host="0.0.0.0", port=5000)
