from app.model import Model

from sqlalchemy.dialects.postgresql import UUID
from uuid import uuid4
from sqlalchemy import Column, String


class Admin(Model):
    __tablename__ = 'admins'

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid4)
    name = Column(String(), nullable=False)

    def __init__(self, name):
        self.name = name
