from sqlalchemy.orm import relationship

from app.model import Model

from sqlalchemy.dialects.postgresql import UUID
from uuid import uuid4
from sqlalchemy import Column, ForeignKey, Numeric


class Result(Model):
    __tablename__ = 'results'

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid4)

    coverage = Column(Numeric(), default=0)

    clicks_count = Column(Numeric(), default=0)

    # relationships
    adcompanyid = Column(UUID(True), ForeignKey("ad_companies.id"), nullable=False)
    adcompany = relationship('AdCompany', back_populates="result", lazy=True)

    clicks = relationship('Click', back_populates="result", cascade="all,delete", lazy=True)
    leads = relationship('Lead', back_populates="result", cascade="all,delete", lazy=True)

