from sqlalchemy.orm import relationship

from app.model import Model

from sqlalchemy.dialects.postgresql import UUID
from uuid import uuid4
from sqlalchemy import Column, String, Boolean, Enum, Numeric, ForeignKey
from app.model.enum import UserRole


class Business(Model):
    __tablename__ = 'businesses'

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid4)

    site = Column(String(), nullable=True)

    fill_form = Column(Boolean(), default=False)

    link = Column(String(), nullable=True)

    role = Column(Enum(UserRole), default=UserRole.BUSINESS)

    blogger_activation = Column(String(), nullable=False, default="")

    payment_for_result = Column(String(), nullable=False, default="")

    balance = Column(Numeric(), nullable=False, default=0)

    name = Column(String(), nullable=False)

    adcompanies = relationship('AdCompany', back_populates="business", cascade="all,delete", lazy=True)
    payments = relationship('Payment', back_populates="business", cascade="all,delete", lazy=True)

    requisites_id = Column(UUID(True), ForeignKey("requisites.id"), nullable=True)
    requisites = relationship('Requisites', cascade="all,delete", back_populates="business", lazy=True)

    def __init__(self, name, site):
        self.name = name
        self.site = site
