from sqlalchemy.orm import relationship

from app.model import Model

from sqlalchemy.dialects.postgresql import UUID
from uuid import uuid4
from sqlalchemy import Column, ForeignKey, TIMESTAMP
from datetime import datetime


class Link(Model):
    __tablename__ = 'links'

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid4)

    created_at = Column(TIMESTAMP(timezone=True), default=datetime.now)

    approved_at = Column(TIMESTAMP(timezone=True), nullable=True)

    def __init__(self, nick, link):
        self.nick = nick
        self.link = link
