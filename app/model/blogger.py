from sqlalchemy.orm import relationship

from app.model import Model

from sqlalchemy.dialects.postgresql import UUID
from uuid import uuid4
from sqlalchemy import Column, String, ForeignKey, Numeric, Enum

from app.model.enum import SocialMedia


class Blogger(Model):
    __tablename__ = 'bloggers'

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid4)

    name = Column(String(), nullable=False)

    link = Column(String(), nullable=False)

    rating = Column(Numeric(), default=0)

    social_media = Column(Enum(SocialMedia), default=SocialMedia.TWITTER)

    subscribers_count = Column(Numeric(), default=0)

    engagement_rate = Column(Numeric(), default=0)

    price_for_post = Column(Numeric(), default=0)

    price_for_post_plus_pin = Column(Numeric(), default=0)

    balance = Column(Numeric(), nullable=False, default=0)

    adcompanies = relationship('AdCompany', back_populates="blogger", cascade="all,delete", lazy=True)

    def __init__(self, name, link, social_media):
        self.name = name
        self.link = link
        self.social_media = social_media
