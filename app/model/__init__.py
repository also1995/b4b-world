from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from app.config import config

SQLALCHEMY_DATABASE_URL = config.DB_URL

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={}
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Model = declarative_base()

from .user import User
from .blogger import Blogger
from .business import Business
from .admin import Admin

from .payment import Payment
from .requisites import Requisites
from .result import Result
from .adcompany import AdCompany

from .click import Click
from .lead import Lead
from .link import Link

Model.metadata.create_all(bind=engine)


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
