from typing import Optional

from sqlalchemy import Column, String, Enum
from sqlalchemy.dialects.postgresql import UUID

from uuid import uuid4

from app.model import Model
from app.model.enum import UserRole


class User(Model):
    __tablename__ = 'users'

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid4)

    email = Column(String(), nullable=False)

    password = Column(String(), nullable=False)

    role = Column(Enum(UserRole), default=UserRole.BUSINESS)

    external_id = Column(UUID(as_uuid=True), nullable=False)

    def __init__(self, email: str, password: str, role: UserRole = UserRole.BUSINESS.value):
        self.email = email
        self.password = password
        self.role = role
