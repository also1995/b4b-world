from sqlalchemy.orm import relationship

from app.model import Model

from sqlalchemy.dialects.postgresql import UUID
from uuid import uuid4
from sqlalchemy import Column, String, ForeignKey, TIMESTAMP
from datetime import datetime


class Click(Model):
    __tablename__ = 'clicks'

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid4)
    date = Column(TIMESTAMP(timezone=True), default=datetime.now)

    # relationships
    result_id = Column(UUID(True), ForeignKey("results.id"), nullable=False)
    result = relationship('Result', back_populates="clicks", lazy=True)

    def __init__(self, result):
        self.result = result
