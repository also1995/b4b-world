from sqlalchemy.orm import relationship

from app.model import Model

from sqlalchemy.dialects.postgresql import UUID
from uuid import uuid4
from sqlalchemy import Column, String


class Requisites(Model):
    __tablename__ = 'requisites'

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid4)

    value = Column(String(), nullable=False)

    business = relationship('Business', back_populates="requisites", uselist=False, cascade="all,delete", lazy=True)

    def __init__(self, value):
        self.value = value
