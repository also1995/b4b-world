from sqlalchemy.orm import relationship

from app.model import Model

from sqlalchemy.dialects.postgresql import UUID
from uuid import uuid4
from sqlalchemy import Column, String, TIMESTAMP, ForeignKey, Numeric, Enum
from datetime import datetime

from app.model.enum import AdFormat, AdStatus


class AdCompany(Model):
    __tablename__ = 'ad_companies'

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid4)

    name = Column(String(), nullable=False)

    publication_date = Column(TIMESTAMP(timezone=True), default=datetime.now)

    comment = Column(String(), nullable=True)

    promocode = Column(String(), nullable=False, default="")

    blogger_result_link = Column(String(), nullable=True)

    utm_link = Column(String(), nullable=True)

    description = Column(String(), nullable=True)

    format = Column(Enum(AdFormat), default=AdFormat.PIN)

    status = Column(Enum(AdStatus), default=AdStatus.WAIT)

    site = Column(String(), nullable=False)

    price = Column(Numeric(), default=0, nullable=False)

    creation_date = Column(TIMESTAMP(timezone=True), default=datetime.now)

    # relationships
    business = relationship('Business', back_populates="adcompanies", uselist=False, lazy=True)
    business_id = Column(UUID(True), ForeignKey("businesses.id"), nullable=False)

    blogger = relationship('Blogger', back_populates="adcompanies", uselist=False, lazy=True)
    blogger_id = Column(UUID(True), ForeignKey("bloggers.id"), nullable=False)

    result = relationship('Result', back_populates="adcompany", uselist=False, cascade="all,delete", lazy=True)

    def __init__(self, name, date, description, ad_format, site, price):
        self.name = name
        self.publication_date = date
        self.description = description
        self.format = ad_format
        self.site = site
        self.price = price
