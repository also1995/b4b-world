from sqlalchemy.orm import relationship

from app.model import Model

from sqlalchemy.dialects.postgresql import UUID
from uuid import uuid4
from sqlalchemy import Column, String, ForeignKey, TIMESTAMP, Enum
from datetime import datetime
from app.model.enum import LeadStatus


class Lead(Model):
    __tablename__ = 'leads'

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid4)

    phone = Column(String(), nullable=False)

    name = Column(String(), nullable=False)

    admin_comment = Column(String(), nullable=True)

    user_comment = Column(String(), nullable=False)

    status = Column(Enum(LeadStatus), nullable=False)

    creation_date = Column(TIMESTAMP(timezone=True), default=datetime.now)

    # relationships
    result_id = Column(UUID(True), ForeignKey("results.id"), nullable=False)
    result = relationship('Result', back_populates="leads", lazy=True)

    def __init__(self, name, phone):
        self.name = name
        self.phone = phone
        self.user_comment = ""
        self.status = LeadStatus.NEW
