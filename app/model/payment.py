from datetime import datetime
from typing import Optional

from sqlalchemy.orm import relationship

from app.model import Model
from app.model.enum import PaymentsStatus

from sqlalchemy.dialects.postgresql import UUID
from uuid import uuid4
from sqlalchemy import Column, Float, TIMESTAMP, Enum as SqlEnum, ForeignKey, String


class Payment(Model):
    __tablename__ = 'payments'

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid4)

    value = Column(Float(), nullable=False)

    created_at = Column(TIMESTAMP(timezone=True), default=datetime.now)

    approved_at = Column(TIMESTAMP(timezone=True), nullable=True)

    status = Column(SqlEnum(PaymentsStatus), default=PaymentsStatus.APPROVED)

    comment = Column(String(), nullable=True)

    # relationships
    business_id = Column(UUID(True), ForeignKey("businesses.id"), nullable=False)
    business = relationship('Business', back_populates="payments", lazy=True)

    def __init__(self, value: float, user_id: Optional[str], comment: Optional[str]):
        self.value = value
        self.business_id = user_id
        self.comment = comment
