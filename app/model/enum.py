from enum import Enum


class PaymentsStatus(Enum):
    APPROVED = "APPROVED"
    REJECTED = "REJECTED"
    WAITING = "WAITING"

    def __str__(self):
        return self.value


class LeadStatus(Enum):
    ACCEPTED = "ACCEPTED"
    IN_PROGRESS = "IN_PROGRESS"
    ERROR = "ERROR"
    NEW = "NEW"

    def __str__(self):
        return self.value


class UserRole(Enum):
    BUSINESS = "BUSINESS"
    ADMIN = "ADMIN"
    BLOGGER = "BLOGGER"

    def __str__(self):
        return self.value


class SocialMedia(Enum):
    TWITTER = "TWITTER"
    TELEGRAM = "TELEGRAM"
    INSTAGRAM = "INSTAGRAM"
    TIKTOK = "TIKTOK"


class AdFormat(Enum):
    PIN = "PIN"
    POST_AND_PIN = "POST_AND_PIN"


class AdStatus(Enum):
    WAIT_LINK = "WAIT_LINK"
    POSTED = "POSTED"
    CONFIRMED = "CONFIRMED"
    APPROVED = "APPROVED"
    WAIT = "WAIT"


class BloggersCount(Enum):
    MINI = "MINI"
    MIDDLE = "MIDDLE"
    MACRO = "MACRO"
