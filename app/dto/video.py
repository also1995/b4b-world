from dataclasses import dataclass
from typing import Any, List


@dataclass
class Video:
    promoCode: str
    viewCount: int
    clicks: int
    contacts: int
    payment: int
    content: Any
    link: str


@dataclass
class VideoData:
    data: List[Video]
    viewCount: int
    contacts: int
    clicks: int
