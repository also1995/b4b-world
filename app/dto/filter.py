from enum import Enum


class TaskFilter(Enum):
    ID = "ID"
    PROBLEM_ID = "PROBLEM_ID"
    STATUS = "STATUS"

    def __str__(self):
        return self.value


class ProblemFilter(Enum):
    ID = "ID"
    NAME = "NAME"
    DOCKER_NAME = "DOCKER_NAME"
    STATUS = "STATUS"

    def __str__(self):
        return self.value
