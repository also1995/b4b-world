from dataclasses import dataclass
from datetime import datetime
from typing import Optional, List
from app.model.enum import LeadStatus, AdStatus, AdFormat


@dataclass
class BloggerDTO:
    id: str
    nick: str
    link: str


@dataclass
class ResultDTO:
    coverage: int
    clicks: int
    leads: int


@dataclass
class LeadDTO:
    id: str
    phone: str
    name: str
    creationDate: datetime
    blogger: BloggerDTO
    status: LeadStatus
    comment: str
    adminComment: str


@dataclass
class LeadsStatisticItem:
    status: LeadStatus
    value: int


@dataclass
class AdCompanyDTO:
    id: str
    name: str
    blogger: BloggerDTO
    publicationDate: datetime
    comment: str
    site: str
    resultLink: str
    result: ResultDTO
    promoCode: str
    utmLink: str
    cost: float
    status: AdStatus
    adFormat: AdFormat
    leadsStatistic: Optional[List[LeadsStatisticItem]] = None

