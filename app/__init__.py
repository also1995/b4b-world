from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from app.config import config

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:8080",
    "http://localhost:3000",
    "http://b4b.app",
    "https://b4b.app",
    "https://www.b4b.app",
    "http://www.b4b.app"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

from .model import *
from .schemas import *
from .views.views import *

