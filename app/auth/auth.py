from datetime import datetime, timedelta
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError, jwt
from passlib.context import CryptContext
from sqlalchemy.orm import Session

from app.model.enum import UserRole
from app.model.user import User as ModelUser
from app.model.business import Business
from app.model.blogger import Blogger
from app.model.admin import Admin


SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 60


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return pwd_context.hash(password)


def authenticate_user(db: Session, email: str, password: str):
    user: ModelUser = db.query(ModelUser).filter_by(email=email).one()
    if user is None:
        return None
    if user.role == UserRole.BUSINESS:
        business: Business = db.query(Business).filter_by(id=user.external_id).one()
        if verify_password(password, user.password):
            return {
                "role": str(user.role),
                "id": str(user.id),
                "entity": {
                    "id": str(business.id),
                    "name": str(business.name)
                }
            }
    elif user.role == UserRole.BLOGGER:
        blogger: Blogger = db.query(Blogger).filter_by(id=user.external_id).one()
        if verify_password(password, user.password):
            return {
                "role": str(user.role),
                "id": str(user.id),
                "entity": {
                    "id": str(blogger.id),
                    "name": str(blogger.name)
                }
            }
    else:
        admin: Admin = db.query(Admin).filter_by(id=user.external_id).one()
        if verify_password(password, user.password):
            return {
                "role": str(user.role),
                "id": str(user.id),
                "entity": {
                    "id": str(admin.id),
                    "name": str(admin.name)
                }
            }

    return None


def create_access_token(data: dict):
    to_encode = data.copy()
    expire = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


async def get_current_user(token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        user = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        if user is None:
            raise credentials_exception
    except JWTError:
        raise credentials_exception
    if user is None:
        raise credentials_exception
    return user
