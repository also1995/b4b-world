from app.model.task import TaskStatus
from app.model.problem import ProblemStatus
from datetime import datetime


def get_task_duration(task):
    time_delta = None

    if task.status == TaskStatus.RUNNING:
        time_delta = datetime.now(tz=task.created_at.tzinfo) - task.queued_at
    if task.status == TaskStatus.ERROR:
        time_delta = task.completed_at - task.queued_at
    if task.status == TaskStatus.COMPLETED:
        time_delta = task.completed_at - task.running_at
    if task.status == TaskStatus.STOPPED:
        time_delta = task.stopped_at - task.queued_at

    return get_time_delta(time_delta)


def get_problem_duration(problem):
    time_delta = None

    if problem.status == ProblemStatus.RUNNING:
        time_delta = datetime.now(tz=problem.created_at.tzinfo) - problem.running_at
    if problem.status == ProblemStatus.COMPLETED:
        time_delta = problem.completed_at - problem.running_at

    return get_time_delta(time_delta)


def get_time_delta(time_delta):
    delta = None

    if time_delta is not None:
        seconds = time_delta.seconds + time_delta.days * 24 * 60 * 60

        hours = seconds // (60 * 60)
        minutes = (seconds - hours * 60 * 60) // 60
        seconds = seconds - hours * 60 * 60 - minutes * 60

        delta = {
            "hours": hours,
            "minutes": minutes,
            "seconds": seconds
        }

    return delta
