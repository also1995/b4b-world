import pandas as pd
import os

df = pd.read_csv(os.path.join(os.path.dirname(__file__), "bloggers_seed.csv"))


def return_seeded_values():
    result = []
    for row in df.to_dict(orient='records'):
        result.append({
            "influencer": row.get(" Influencer"),
            "link": row.get("Link to the Influencer"),
            "raiting": row.get("Rating B4B Points"),
            "subscribers": row.get('Followers'),
            "price": row.get("Price"),
            "followers": row.get("Followers"),
            "er": row.get("ER"),
            "pricePin": row.get("PricePin"),
            "socialMedia": row.get("Social Media"),
        })
    return result
