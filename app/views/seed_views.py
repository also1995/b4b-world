from fastapi import Depends
from sqlalchemy.orm import Session

from app import app
from app.model import get_db, User, Blogger, Business, Admin
from app.model.enum import UserRole, SocialMedia
from app.auth.auth import get_password_hash
from app.seed.seed import return_seeded_values


def get_social_media(name):
    if name == "Telegram":
        return SocialMedia.TELEGRAM
    return SocialMedia.TWITTER


@app.get("/api/seed")
def seed(db: Session = Depends(get_db)):
    user = User("alexey@allright.com", get_password_hash("1"), UserRole.BUSINESS)
    business = Business("AllRight", "https://allright.com/")
    db.add(business)
    db.flush()
    db.refresh(business)
    user.external_id = business.id

    admin = User("admin@admin.com", get_password_hash("1"), UserRole.ADMIN)
    admin_entity = Admin("Admin")
    db.add(admin_entity)
    db.flush()
    db.refresh(admin_entity)

    admin.external_id = admin_entity.id

    db.add(admin)
    db.add(user)

    seeded_bloggers = return_seeded_values()

    for blogger in seeded_bloggers:
        user = User(blogger.get("influencer").lower() + "@gmail.com", get_password_hash("1"), UserRole.BLOGGER)
        blogger_entity = Blogger(blogger.get("influencer"), blogger.get("link"), get_social_media(blogger.get("socialMedia")))
        blogger_entity.rating = int(blogger.get("raiting"))
        blogger_entity.price_for_post = float(blogger.get("price"))
        blogger_entity.price_for_post_plus_pin = float(blogger.get("pricePin").replace(',', ''))
        blogger_entity.engagement_rate = float(blogger.get("er").replace("%", ""))
        blogger_entity.subscribers_count = int(blogger.get("subscribers"))

        db.add(blogger_entity)
        db.flush()
        db.refresh(blogger_entity)

        user.external_id = blogger_entity.id

        db.add(user)

        db.commit()
    db.commit()
