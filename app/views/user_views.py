from fastapi import Depends, HTTPException, status
from fastapi.responses import JSONResponse
from sqlalchemy.orm import Session

from typing import List
from app import app
from app.model.business import Business
from app.model.blogger import Blogger
from app.model import get_db
from app.model.user import User as ModelUser
from app.model.enum import UserRole
from app.schemas.user import Token, BusinessUser, UserLogin, UpdateUserParameters, BloggerUser
from app.auth.auth import authenticate_user, create_access_token, get_password_hash, get_current_user


@app.post("/api/users/login", response_model=Token)
async def login(form_data: UserLogin, db: Session = Depends(get_db)):
    user = authenticate_user(db, form_data.email, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect email or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token = create_access_token(data=user)
    return {"access_token": access_token, "token_type": "bearer", "user": user}


@app.post("/api/users/business-registration")
def business_registration(user: BusinessUser, db: Session = Depends(get_db)):
    model_user: ModelUser = ModelUser(
            email=user.email,
            password=get_password_hash(user.password),
            role=UserRole.BUSINESS.value
    )
    business: Business = Business(name=user.name, site=user.site)
    db.add(business)
    db.flush()
    db.refresh(business)

    model_user.external_id = business.id
    db.add(model_user)
    db.commit()

    return JSONResponse(status_code=status.HTTP_200_OK)


@app.post("/api/users/blogger-registration")
def blogger_registration(user: BloggerUser, db: Session = Depends(get_db)):
    model_user: ModelUser = ModelUser(
        email=user.email,
        password=get_password_hash(user.password),
        role=UserRole.BLOGGER.value
    )
    blogger: Blogger = Blogger(name=user.name, link=user.link, social_media=user.socialMedia)
    db.add(blogger)
    db.flush()
    db.refresh(blogger)

    model_user.external_id = blogger.id
    db.add(model_user)
    db.commit()

    return JSONResponse(status_code=status.HTTP_200_OK)


@app.post("/api/users/update-financial-parameters")
async def update_financial_parameters(params: UpdateUserParameters,  user: dict = Depends(get_current_user), db: Session = Depends(get_db)):
    user = db.query(ModelUser).filter_by(id=params.businessId).one()

    user.blogger_activation = params.bloggerActivation
    user.payment_for_result = params.paymentForResult

    db.add(user)
    db.commit()

    return JSONResponse(status_code=status.HTTP_200_OK)


@app.get("/api/users/current")
def get_current_user(user: dict = Depends(get_current_user)):
    return user


@app.get("/api/users")
def get_users(user: dict = Depends(get_current_user), db: Session = Depends(get_db)):
    users = db.query(ModelUser).all()
    result = list()
    for user in users:
        result.append({"id": user.id, "name": user.name})

    return result
