from app import app, AdCompany
from fastapi import Depends
from typing import List

from app.auth.auth import get_current_user
from sqlalchemy.orm import Session
from app.model import get_db, Blogger
from app.model.enum import SocialMedia, BloggersCount
from app.schemas.blogger import BloggerFilter
from app.schemas.blogger import BLogger as BloggerSchema


@app.post("/api/bloggers")
def bloggers(filter: BloggerFilter, user: dict = Depends(get_current_user), db: Session = Depends(get_db)):
    query = db.query(Blogger)
    if filter.socialMedia is not None and len(filter.socialMedia) != 0:
        query = query.filter(Blogger.social_media.in_(filter.socialMedia))
    if filter.subscribersCount is not None:
        if filter.subscribersCount == BloggersCount.MINI:
            print("Mini")
            query = query.filter(Blogger.subscribers_count.__le__(10**5))
        if filter.subscribersCount == BloggersCount.MIDDLE:
            print("Middle")
            query = query.filter(Blogger.subscribers_count.__ge__(10**5)).filter(Blogger.subscribers_count.__le__(9*(10**5)))
        if filter.subscribersCount == BloggersCount.MACRO:
            print("Macro")
            query = query.filter(Blogger.subscribers_count.__ge__(9*(10 ** 5)))

    bloggers: List[Blogger] = query.all()

    return [BloggerSchema(id=str(blogger.id),
                          name=blogger.name,
                          raiting=blogger.rating,
                          socialMedia=blogger.social_media.value,
                          subscribers=blogger.subscribers_count,
                          er=blogger.engagement_rate,
                          price=blogger.price_for_post,
                          pricePin=blogger.price_for_post_plus_pin
                          ) for blogger in bloggers]


