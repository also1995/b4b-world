from sqlalchemy.orm import Session
from fastapi import Depends, status
from fastapi.responses import JSONResponse
from typing import List

from app import app
from app.model import get_db, Blogger, AdCompany, Business, Result, Lead
from app.schemas.adcompany import CreateAdCompany, EditAdCompany, GetAdCompany
from app.auth.auth import get_current_user
from app.model.enum import LeadStatus, AdFormat, AdStatus

from app.dto.adcompany import AdCompanyDTO, BloggerDTO, ResultDTO, LeadsStatisticItem


@app.post("/api/ad-companies/create")
def create_ad_company(data: CreateAdCompany, db: Session = Depends(get_db), user: dict = Depends(get_current_user)):

    business = db.query(Business).filter_by(id=user.get("entity").get("id")).one_or_none()
    blogger = db.query(Blogger).filter_by(id=data.bloggerId).one_or_none()

    if data.adFormat == AdFormat.PIN:
        price = blogger.price_for_post
    else:
        price = blogger.price_for_post_plus_pin


    adcompany = AdCompany(business.name, data.date, data.description, data.adFormat, business.site, price)

    adcompany.business = business
    adcompany.blogger = blogger
    adcompany.result = Result()

    db.add(adcompany)
    db.commit()

    return JSONResponse(status_code=status.HTTP_200_OK)


@app.post("/api/ad-companies/edit")
def edit_ad_company(data: EditAdCompany, db: Session = Depends(get_db), user: dict = Depends(get_current_user)):
    item: AdCompany = db.query(AdCompany).filter_by(id=data.id).one()
    item.publication_date = data.publicationDate
    item.comment = data.comment
    item.result.coverage = data.coverage
    item.result.clicks_count = data.clicks
    item.promocode = data.promoCode
    item.blogger_result_link = data.site
    item.utm_link = data.utmLink
    item.price = data.cost

    db.add(item)
    db.commit()

    return AdCompanyDTO(id=item.id,
                        name=item.name,
                        blogger=BloggerDTO(
                            id=item.blogger.id,
                            nick=item.blogger.name,
                            link=item.blogger.link
                        ),
                        result=ResultDTO(
                            coverage=item.result.coverage,
                            clicks=item.result.clicks_count,
                            leads=len(item.result.leads)
                        ),
                        publicationDate=item.publication_date,
                        site=item.site,
                        resultLink=item.blogger_result_link,
                        comment=item.comment,
                        promoCode=item.promocode,
                        utmLink=item.utm_link,
                        cost=item.price,
                        adFormat=item.format.value,
                        status=item.status
                        )


@app.get("/api/ad-companies/{company_id}")
def get_ad_company(company_id: str, db: Session = Depends(get_db), user: dict = Depends(get_current_user)):
    item: AdCompany = db.query(AdCompany).filter_by(id=company_id).one()
    result: Result = item.result

    in_progress = db.query(Lead).filter_by(result_id=result.id, status=LeadStatus.IN_PROGRESS).count()
    error = db.query(Lead).filter_by(result_id=result.id, status=LeadStatus.ERROR).count()
    accepted = db.query(Lead).filter_by(result_id=result.id, status=LeadStatus.ACCEPTED).count()
    new = db.query(Lead).filter_by(result_id=result.id, status=LeadStatus.NEW).count()

    return AdCompanyDTO(id=item.id,
                        name=item.name,
                        blogger=BloggerDTO(
                            id=item.blogger.id,
                            nick=item.blogger.name,
                            link=item.blogger.link
                        ),
                        result=ResultDTO(
                            coverage=item.result.coverage,
                            clicks=item.result.clicks_count,
                            leads=len(item.result.leads)
                        ),
                        publicationDate=item.publication_date,
                        site=item.site,
                        resultLink=item.blogger_result_link,
                        comment=item.comment,
                        promoCode=item.promocode,
                        leadsStatistic=[
                            LeadsStatisticItem(status=LeadStatus.IN_PROGRESS, value=in_progress),
                            LeadsStatisticItem(status=LeadStatus.ERROR, value=error),
                            LeadsStatisticItem(status=LeadStatus.ACCEPTED, value=accepted),
                            LeadsStatisticItem(status=LeadStatus.NEW, value=new)
                        ],
                        utmLink=item.utm_link,
                        cost=item.price,
                        adFormat=item.format.value,
                        status=item.status
                        )


@app.post("/api/ad-companies/in-progress")
def get_adcompanies_in_progress(data: GetAdCompany, db: Session = Depends(get_db), user: dict = Depends(get_current_user)):
    items = db.query(AdCompany).filter_by(business_id=data.businessId).filter(AdCompany.status != AdStatus.APPROVED).order_by(AdCompany.creation_date.desc()).all()
    result = []

    for item in items:
        result.append(
            AdCompanyDTO(id=item.id,
                         cost=item.price,
                         name=item.name,
                         blogger=BloggerDTO(
                             id=item.blogger.id,
                             nick=item.blogger.name,
                             link=item.blogger.link
                         ),
                         result=ResultDTO(
                             coverage=item.result.coverage,
                             clicks=item.result.clicks_count,
                             leads=len(item.result.leads)
                         ),
                         publicationDate=item.publication_date,
                         site=item.site,
                         resultLink=item.blogger_result_link,
                         comment=item.comment,
                         promoCode=item.promocode,
                         utmLink=item.utm_link,
                         adFormat=item.format.value,
                         status=item.status
                         )
        )
    return result


@app.post("/api/ad-companies")
def get_adcompanies(data: GetAdCompany, db: Session = Depends(get_db), user: dict = Depends(get_current_user)):
    if data.businessId is None:
        items: List[AdCompany] = db.query(AdCompany).filter_by(business_id=user.get("id")).filter(AdCompany.status == AdStatus.APPROVED).order_by(AdCompany.creation_date.desc()).all()
    else:
        items: List[AdCompany] = db.query(AdCompany).filter_by(business_id=data.businessId).filter(AdCompany.status == AdStatus.APPROVED).order_by(AdCompany.creation_date.desc()).all()

    result = []

    clicks = 0
    coverage = 0
    leads = 0

    for item in items:
        result.append(
            AdCompanyDTO(id=item.id,
                         cost=item.price,
                         name=item.name,
                         blogger=BloggerDTO(
                            id=item.blogger.id,
                            nick=item.blogger.name,
                            link=item.blogger.link
                         ),
                         result=ResultDTO(
                             coverage=item.result.coverage,
                             clicks=item.result.clicks_count,
                             leads=len(item.result.leads)
                         ),
                         publicationDate=item.publication_date,
                         site=item.site,
                         resultLink=item.blogger_result_link,
                         comment=item.comment,
                         promoCode=item.promocode,
                         utmLink=item.utm_link,
                         adFormat=item.format.value,
                         status=item.status
                         )
        )

    for res in result:
        clicks += res.result.clicks
        coverage += res.result.coverage
        leads += res.result.leads

    return {
        "data": result,
        "clicks": clicks,
        "coverage": coverage,
        "leads": leads
    }


@app.delete('/api/ad-companies/{company_id}')
def delete_company(company_id: str, user: dict = Depends(get_current_user), db: Session = Depends(get_db)):
    company = db.query(AdCompany).filter_by(id=company_id).one()

    db.delete(company)
    db.commit()

    return JSONResponse(status_code=status.HTTP_200_OK)


@app.post('/api/ad-companies/{company_id}/approve')
def approve_company(company_id: str, user: dict = Depends(get_current_user), db: Session = Depends(get_db)):
    company: AdCompany = db.query(AdCompany).filter_by(id=company_id).one()
    company.status = AdStatus.APPROVED

    db.add(company)
    db.commit()

    return JSONResponse(status_code=status.HTTP_200_OK)