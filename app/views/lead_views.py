from fastapi import Depends
from fastapi import status, HTTPException
from sqlalchemy.orm import Session
from fastapi.responses import JSONResponse


from app import app
from app.model import get_db, AdCompany, Result, Lead, Blogger
from app.model.enum import LeadStatus
from app.schemas.lead import CreateLead, UpdateLead
from app.auth.auth import get_current_user
from app.dto.adcompany import LeadDTO, BloggerDTO


@app.get("/api/leads/{company_id}")
def get_leads(company_id: str, db: Session = Depends(get_db), user: dict = Depends(get_current_user)):
    leads = list()

    res = db.query(AdCompany, Result, Lead, Blogger).filter(
        AdCompany.id == company_id
    ).filter(
        AdCompany.blogger_id == Blogger.id
    ).filter(
        AdCompany.id == Result.adcompanyid
    ).filter(
        Lead.result_id == Result.id
    ).order_by(Lead.creation_date.desc()).all()

    for r in res:
        _status = r[2].status
        if _status is None:
            _status = LeadStatus.NEW
        leads.append(
            LeadDTO(
                id=str(r[2].id),
                name=r[2].name,
                phone=r[2].phone,
                creationDate=r[2].creation_date,
                blogger=BloggerDTO(
                    id=str(r[3].id),
                    nick=r[3].name,
                    link=r[3].link
                ),
                status=_status,
                comment=r[2].user_comment,
                adminComment=r[2].admin_comment
            )
        )

    return leads


@app.post("/api/leads/{lead_id}/accept")
def lead_accepted(lead_id: str, db: Session = Depends(get_db), user: dict = Depends(get_current_user)):
    lead = db.query(Lead).filter_by(id=lead_id).one()
    lead.status = LeadStatus.ACCEPTED

    db.add(lead)
    db.commit()

    return JSONResponse(status_code=status.HTTP_200_OK)


@app.post("/api/leads/{lead_id}/reset")
def lead_reset(lead_id: str, db: Session = Depends(get_db), user: dict = Depends(get_current_user)):
    lead = db.query(Lead).filter_by(id=lead_id).one()
    lead.status = None

    db.add(lead)
    db.commit()

    return JSONResponse(status_code=status.HTTP_200_OK)


@app.post("/api/leads/{lead_id}/error")
def lead_error(lead_id: str, db: Session = Depends(get_db), user: dict = Depends(get_current_user)):
    lead = db.query(Lead).filter_by(id=lead_id).one()
    lead.status = LeadStatus.ERROR

    db.add(lead)
    db.commit()

    return JSONResponse(status_code=status.HTTP_200_OK)


@app.post("/api/leads/{lead_id}/in-progress")
def lead_in_progress(lead_id: str, db: Session = Depends(get_db), user: dict = Depends(get_current_user)):
    lead = db.query(Lead).filter_by(id=lead_id).one()
    lead.status = LeadStatus.IN_PROGRESS

    db.add(lead)
    db.commit()

    return JSONResponse(status_code=status.HTTP_200_OK)


# @app.post("/api/leads/create")
# def create_lead(lead: CreateLead, db: Session = Depends(get_db), user: dict = Depends(get_current_user)):
#     form = db.query(Form).filter_by(id=lead.formId).one()
#
#     if form is None:
#         raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Form not found")
#
#     lead_model = Lead(lead.name, lead.phone)
#     lead_model.result = form.adcompany.result
#
#     db.add(lead_model)
#     db.commit()
#
#     return JSONResponse(status_code=status.HTTP_200_OK)


@app.post("/api/leads/create/{business_id}")
def create_lead_for_business(business_id: str, lead: CreateLead, db: Session = Depends(get_db), user: dict = Depends(get_current_user)):
    business = db.query(AdCompany).filter_by(id=business_id).one()

    if business is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Business is not found")

    lead_model = Lead(lead.name, lead.phone)
    lead_model.result = business.result

    db.add(lead_model)
    db.commit()

    return JSONResponse(status_code=status.HTTP_200_OK)


@app.post("/api/leads/update")
def update_lead(_update_lead: UpdateLead, db: Session = Depends(get_db), user: dict = Depends(get_current_user)):
    lead = db.query(Lead).filter_by(id=_update_lead.id).one()

    lead.name = _update_lead.name
    lead.phone = _update_lead.phone
    if _update_lead.type == "ADMIN":
        lead.admin_comment = _update_lead.comment
    else:
        lead.user_comment = _update_lead.comment

    db.add(lead)
    db.commit()

    return JSONResponse(status_code=status.HTTP_200_OK)


@app.delete('/api/leads/{lead_id}')
def delete_business(lead_id: str, user: dict = Depends(get_current_user), db: Session = Depends(get_db)):
    lead = db.query(Lead).filter_by(id=lead_id).one()

    db.delete(lead)
    db.commit()

    return JSONResponse(status_code=status.HTTP_200_OK)

