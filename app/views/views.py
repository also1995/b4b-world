from app import app


@app.get('/')
def index():
    return "Server"

from .user_views import *
from .lead_views import *
from .seed_views import *
from .operation_views import *
from .ad_company_views import *
from .business_views import *
from .blogger_views import *
