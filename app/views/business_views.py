from fastapi import Depends, status
from fastapi.responses import JSONResponse
from sqlalchemy.orm import Session

from typing import List
from app import app
from app.model.business import Business
from app.model import get_db
from app.model.user import User as ModelUser
from app.model.enum import UserRole
from app.auth.auth import get_current_user


@app.get("/api/businesses")
def get_businesses(user: dict = Depends(get_current_user), db: Session = Depends(get_db)):
    businesses: List[Business] = db.query(Business).all()

    res = list()

    for u in businesses:
        res.append({
            "id": u.id,
            "site": u.site,
            "deposit": u.balance,
            "name": u.name,
            "paymentForResult": u.payment_for_result,
            "bloggerActivation": u.blogger_activation
        })

    return res


@app.get("/api/businesses/{business_id}")
def get_businesses(business_id: str, user: dict = Depends(get_current_user), db: Session = Depends(get_db)):
    user = db.query(Business).filter_by(id=business_id).one()

    return {
        "id": user.id,
        "site": user.site,
        "deposit": user.balance,
        "name": user.name,
        "paymentForResult": user.payment_for_result,
        "bloggerActivation": user.blogger_activation
    }


@app.delete('/api/businesses/{business_id}')
def delete_business(business_id: str, user: dict = Depends(get_current_user), db: Session = Depends(get_db)):
    business = db.query(Business).filter_by(id=business_id).one()
    user = db.query(ModelUser).filter_by(external_id=business_id).one()

    db.delete(business)
    db.delete(user)
    db.commit()

    return JSONResponse(status_code=status.HTTP_200_OK)
