from sqlalchemy.orm import Session
from decimal import Decimal
from fastapi import Depends, status
from fastapi.responses import JSONResponse

from app import app
from app.model import get_db, Payment, User, Business
from app.schemas.data import RequestData, OperationData
from app.auth.auth import get_current_user
from app.schemas.payment import PaymentSchema


@app.post("/api/operations", response_model=OperationData)
def get_operations(data: RequestData, db: Session = Depends(get_db), user: dict = Depends(get_current_user)):
    items = db.query(Payment).filter_by(business_id=data.filters[0].values[0]).order_by(Payment.created_at.desc()).limit(data.count).offset(data.count * (data.page - 1)).all()
    total = db.query(Payment).count()

    return {
        "data": items,
        "total": total
    }


@app.post("/api/add-deposit")
def add_deposit(payment: PaymentSchema, db: Session = Depends(get_db), user: dict = Depends(get_current_user)):
    business = db.query(Business).filter_by(id=payment.businessId).one()
    db.add(Payment(payment.value, payment.businessId, payment.comment))

    business.balance += Decimal(payment.value)
    db.add(business)

    db.commit()

    return JSONResponse(status_code=status.HTTP_200_OK)


@app.delete("/api/deposit/{deposit_id}")
def delete_deposit(deposit_id: str, db: Session = Depends(get_db), user: dict = Depends(get_current_user)):
    deposit = db.query(Payment).filter_by(id=deposit_id).one()
    business = deposit.business
    business.balance -= Decimal(deposit.value)
    db.delete(deposit)
    db.add(business)
    db.commit()
    return JSONResponse(status_code=status.HTTP_200_OK)
