from typing import List, Any
from pydantic import BaseModel

from app.schemas.operation import OperationSchema


class BaseData(BaseModel):
    total: int
    data: List[Any]


class OperationData(BaseData):
    data: List[OperationSchema]


class FilterItem(BaseModel):
    id: str
    values: List[str]


class RequestData(BaseModel):
    count: int
    filters: List[FilterItem]
    page: int

