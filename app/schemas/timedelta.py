from pydantic import BaseModel


class TimeDelta(BaseModel):
    hours: int
    minutes: int
    seconds: int
