from uuid import UUID
from pydantic import BaseModel


class FormSchema(BaseModel):
    id: UUID
    text: str
    link: str

    class Config:
        orm_mode = True
        allow_population_by_field_name = True