from typing import Optional

from pydantic import BaseModel


class PaymentSchema(BaseModel):
    value: float
    businessId: str
    comment: Optional[str]

