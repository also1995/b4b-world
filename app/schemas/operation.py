from typing import Optional
from uuid import UUID

from pydantic import BaseModel, Field
from datetime import datetime
from app.model.enum import PaymentsStatus


class OperationSchema(BaseModel):
    id: UUID
    value: float
    created_at: Optional[datetime] = Field(alias="createdAt")
    approved_at: Optional[datetime] = Field(alias="approvedAt")
    status: PaymentsStatus
    comment: Optional[str]

    class Config:
        orm_mode = True
        allow_population_by_field_name = True