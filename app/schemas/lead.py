from typing import Optional
from uuid import UUID

from pydantic import BaseModel
from datetime import datetime


class BloggerSchema(BaseModel):
    id: str
    nick: str
    link: str


class LeadSchema(BaseModel):
    id: UUID
    phone: str
    name: str
    creationDate: datetime
    blogger: BloggerSchema


class CreateLead(BaseModel):
    phone: str
    name: str
    formId: Optional[str]


class UpdateLead(BaseModel):
    id: str
    phone: str
    name: str
    comment: Optional[str]
    type: str
