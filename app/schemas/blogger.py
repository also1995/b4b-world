from typing import Optional, List

from pydantic import BaseModel
from datetime import datetime
from app.model.enum import BloggersCount


class BloggerFilter(BaseModel):
    socialMedia: Optional[List[str]]
    subscribersCount: Optional[BloggersCount]
    ## two dates
    dateRange: Optional[List[datetime]]


class BLogger(BaseModel):
    id: str
    name: str
    raiting: float
    socialMedia: str
    subscribers: int
    er: float
    price: float
    pricePin: float
