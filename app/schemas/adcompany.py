from typing import Optional

from pydantic import BaseModel
from datetime import datetime

from app.model.enum import AdFormat


class CreateAdCompany(BaseModel):
    date: datetime
    bloggerId: str
    description: str
    adFormat: AdFormat


class GetAdCompany(BaseModel):
    businessId: Optional[str]


class EditAdCompany(BaseModel):
    id: str
    publicationDate: datetime
    comment: Optional[str]
    coverage: int
    promoCode: Optional[str]
    site: Optional[str]
    clicks: int
    cost: int
    utmLink: Optional[str]
