from typing import Optional

from pydantic import BaseModel


class Token(BaseModel):
    access_token: str
    token_type: str
    user: dict


class TokenData(BaseModel):
    username: Optional[str] = None


class BusinessUser(BaseModel):
    name: str
    site: str
    email: str
    password: str


class BloggerUser(BaseModel):
    email: str
    password: str
    link: str
    name: str
    socialMedia: str


class UserLogin(BaseModel):
    email: str
    password: str


class PasswordChange(BaseModel):
    old: str
    new: str


class PasswordReset(BaseModel):
    email: str


class CompleteResetPassword(BaseModel):
    password: str


class UpdateUserParameters(BaseModel):
    bloggerActivation: Optional[str]
    paymentForResult: Optional[str]
    businessId: str
