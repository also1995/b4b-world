"""add blogger activation and payment for result

Revision ID: e74680813902
Revises: 7191b5fa6c95
Create Date: 2021-04-22 21:36:00.998051

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e74680813902'
down_revision = '7191b5fa6c95'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("users", sa.Column("blogger_activation", sa.String(), nullable=True))
    op.add_column("users", sa.Column("payment_for_result", sa.String(), nullable=True))


def downgrade():
    op.drop_column('users', 'blogger_activation')
    op.drop_column('users', 'payment_for_result')

