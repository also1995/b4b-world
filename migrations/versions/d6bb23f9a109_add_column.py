"""add column

Revision ID: d6bb23f9a109
Revises: 
Create Date: 2021-02-26 00:51:00.342379

"""
from alembic import op
import sqlalchemy as sa
from app.model.enum import LeadStatus


# revision identifiers, used by Alembic.
revision = 'd6bb23f9a109'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    #op.execute("CREATE TYPE leadstatus AS ENUM('ACCEPTED', 'IN_PROGRESS', 'ERROR')")
    op.add_column("leads", sa.Column("state", sa.Enum(LeadStatus), nullable=True))


def downgrade():
    op.drop_column('leads', 'state')
    op.execute("DROP TYPE leadstatus")

