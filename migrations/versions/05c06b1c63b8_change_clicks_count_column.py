"""change clicks count column

Revision ID: 05c06b1c63b8
Revises: c65179160774
Create Date: 2021-02-26 01:39:33.689907

"""
from alembic import op
import sqlalchemy as sa
from app.model.enum import LeadStatus


# revision identifiers, used by Alembic.
revision = '05c06b1c63b8'
down_revision = 'c65179160774'
branch_labels = None
depends_on = None


def upgrade():
    op.drop_column('leads', 'state')
    op.add_column("leads", sa.Column("status", sa.Enum(LeadStatus), nullable=True))


def downgrade():
    op.drop_column('results', 'clicks_count')
    op.drop_column('leads', 'status')
    op.add_column("leads", sa.Column("state", sa.Enum(LeadStatus), nullable=True))
