"""change_leads_table

Revision ID: f3e210533075
Revises: 05c06b1c63b8
Create Date: 2021-03-02 23:54:00.388660

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f3e210533075'
down_revision = '05c06b1c63b8'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("leads", sa.Column("admin_comment", sa.String(), nullable=True))
    op.add_column("leads", sa.Column("user_comment", sa.String(), nullable=True))


def downgrade():
    op.drop_column('leads', 'admin_comment')
    op.drop_column('leads', 'user_comment')
