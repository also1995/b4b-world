"""add clicks count column

Revision ID: c65179160774
Revises: d6bb23f9a109
Create Date: 2021-02-26 01:32:45.598348

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c65179160774'
down_revision = 'd6bb23f9a109'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("results", sa.Column("clicks_count", sa.Numeric(), server_default="0"))


def downgrade():
    op.drop_column('results', 'clicks_count')
