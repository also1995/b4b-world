"""create utm link

Revision ID: 7191b5fa6c95
Revises: f3e210533075
Create Date: 2021-03-17 01:36:31.027043

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7191b5fa6c95'
down_revision = 'f3e210533075'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("ad_companies", sa.Column("utm_link", sa.String(), nullable=True))


def downgrade():
    op.drop_column('ad_companies', 'utm_link')

