"""Add cost column

Revision ID: d7225ad1fe03
Revises: e74680813902
Create Date: 2021-06-02 00:04:52.545938

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd7225ad1fe03'
down_revision = 'e74680813902'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("ad_companies", sa.Column("cost", sa.Numeric, nullable=False, server_default="0"))


def downgrade():
    op.drop_column('ad_companies', 'cost')
