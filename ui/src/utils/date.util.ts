import {Duration} from "../models/duration";
import {EMPTY_SIGN} from "../constants/constants";

export function getFullDate(date: string | Date): string {
  if (!date) return EMPTY_SIGN;

  const currDate = new Date(date);
  return currDate.toLocaleString();
}

export function getFullDuration(duration: Duration): string {
  if (!duration) return EMPTY_SIGN;

  const durations: string[] = [];

  if (duration.hours) {
    durations.push(`${duration.hours}h`);
  }

  if (duration.minutes) {
    durations.push(`${duration.minutes}m`);
  }

  if (duration.seconds) {
    durations.push(`${duration.seconds}s`);
  }

  return durations.join("");
}