import React, {useEffect, useState} from "react";
import {User} from "../models/user";
import {useAuth} from "../hooks/use-auth";
import {useNavigate} from "react-router-dom";

type AuthContextProps = {
    logout: Function;
    login: Function;
    currentUser: User | null;
    isAuthorized: boolean;
    setAuthorized: Function;
}

export const AuthContext = React.createContext<AuthContextProps>({
  login: () => {
  },
  logout: () => {
  },
  currentUser: null,
  isAuthorized: true,
  setAuthorized: () => {}
});

interface Props {
  children: React.ReactNode;
}

export const AuthProvider: React.FC<Props> = ({children}) => {
  const {login, logout, getCurrentUser} = useAuth();
  const [currentUser, setCurrentUser] = useState<User | null>(null);
  const [isAuthorized, setAuthorized] = useState<boolean>(true);
  const navigate = useNavigate();

  const loginAction = async (values: any) => {
    try {
      await login(values);
      const user = await getCurrentUser();
      setCurrentUser(user);
      setAuthorized(true);
      navigate("/");
    } catch (e) {
      console.error(e)
      setAuthorized(false);
    }
  }

  const logoutAction = async () => {
    await logout();
    setAuthorized(false);
  }

  useEffect(() => {
    const getUser = async () => {
      try {
        const user = await getCurrentUser();
        setCurrentUser(user);
        setAuthorized(true);
      } catch (e) {
        setAuthorized(false);
      }
    }
    getUser();
  }, [getCurrentUser]);

  return <AuthContext.Provider
    value={{
      logout: logoutAction,
      login: loginAction,
      currentUser,
      isAuthorized,
      setAuthorized
    }}
  >
    {children}
  </AuthContext.Provider>
}
