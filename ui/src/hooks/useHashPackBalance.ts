import { useCallback, useEffect, useState } from 'react';
import { useHashConnect } from './useHashConnect';
import {hashconnectService} from "../influencer/hashconnect";

export const useHashPackBalance = () => {
  const [balance, setBalance] = useState<string>();
  const { isPaired } = useHashConnect();

  const getBalance = useCallback(async () => {
    const obtainedBalance = await hashconnectService.getCurrentAccountBalance();

    setBalance(JSON.parse(obtainedBalance).hbars);
  }, []);

  useEffect(() => {
    if (isPaired) {
      getBalance();
    }
  }, [getBalance, isPaired]);

  return balance;
};
