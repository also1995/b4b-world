import {ApiProvider} from "../providers/api-provider";
import {useContext} from "react";
import {AuthContext} from "../contexts/auth.provider";
import {notification} from "antd";


export const useAPI = () => {
  const {setAuthorized} = useContext(AuthContext);

  async function _executePost<T>(url: string, body: any): Promise<T> {
    try {
      return await ApiProvider.postRequest<T>(url, body);
    } catch (e) {
      catchException(e);
      throw e;
    }
  }

  async function _executeForm<T>(url: string, body: FormData): Promise<T> {
    try {
      return await ApiProvider.postFormData<T>(url, body);
    } catch (e) {
      catchException(e);
      throw e;
    }
  }

  async function _executeDelete<T>(url: string): Promise<T> {
    try {
      return await ApiProvider.deleteRequest<T>(url);
    } catch (e) {
      catchException(e);
      throw e;
    }
  }

  async function _executeGet<T>(url: string): Promise<T> {
    try {
      return ApiProvider.getRequest<T>(url);
    } catch (e) {
      catchException(e);
      throw e;
    }
  }

  function catchException(e: any) {
    notification.error({
      description: e.response ? e.response.data.detail : "Server Error",
      message: "Error"
    });

    if (e.response.status === 401) {
      setAuthorized(false);
    }
  }

  return {

  }
}
