import { useCallback, useEffect, useState } from 'react';
import {HashconnectService, hashconnectService} from "../influencer/hashconnect";

export const useHashConnect = (instance: HashconnectService = hashconnectService) => {
  const [pairingString, setPairingString] = useState<string>();
  const [isPaired, setIsPaired] = useState(false);

  const logStatus = useCallback(() => {
    switch (instance.status) {
      case 'Connected': {
        setPairingString(instance.saveData.pairingString);
        break;
      }
      case 'Paired': {
        instance.getBalanceFromSmartContract()
        setIsPaired(true);
        break;
      }
    }
  }, []);

  useEffect(() => {
    logStatus();
    instance.on('status', logStatus);

    return () => {
      instance.removeListener('status', logStatus);
    };
  }, [logStatus]);

  return {
    pairingString,
    isPaired,
  };
};
