import Cookies from 'js-cookie';
import {ApiProvider} from "../providers/api-provider";

export const USER_COOKIES = 'user';
export const USER_TOKEN_COOKIES = 'token';

async function login(values: any): Promise<any> {
  const payload = await ApiProvider.postRequest<any>("users/login", values);
  Cookies.set(USER_TOKEN_COOKIES, payload.access_token);
  Cookies.set(USER_COOKIES, payload.user);
  return payload.user;
}

async function logout(): Promise<void> {
  Cookies.remove(USER_TOKEN_COOKIES);
  Cookies.remove(USER_COOKIES);
}

async function getCurrentUser(): Promise<any> {
  return await ApiProvider.getRequest<any>("users/current");
}

function getBearer(): string | undefined {
  return Cookies.get(USER_TOKEN_COOKIES);
}

export const useAuth = () => {

  return {
    login,
    logout,
    getCurrentUser,
    getBearer
  }
}
