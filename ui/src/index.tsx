import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import AllApp from './App';

ReactDOM.render(
  <React.StrictMode>
    <AllApp />
  </React.StrictMode>,
  document.getElementById('root')
);
