import React, {useContext, useEffect, useState} from "react";
import "./balance.less";
import {Button, Descriptions, Typography} from "antd";
import {AuthContext} from "../../contexts/auth.provider";

export interface BalanceProps {
    balance: number;
    bloggerActivation?: string;
    paymentForResult?: string;
    onReplenish: Function;
    onEditParams?: Function;
}

export const Bold: React.FC<{ children: React.ReactNode }> = props => {
  return <span style={{fontWeight: 600}}>{props.children}</span>;
}

const {Text} = Typography;

export const Balance: React.FC<BalanceProps> = props => {
  const {currentUser} = useContext(AuthContext);
  const [bloggerActivation, setBloggerActivation] = useState<string>("No Info")
  const [paymentForResult, setPaymentForResult] = useState<string>( "No Info")
  const editable = currentUser?.role === "ADMIN";

  useEffect(() => {
    setBloggerActivation(props.bloggerActivation || "No Info");
    setPaymentForResult(props.paymentForResult || "No Info");
  }, [props.paymentForResult, props.bloggerActivation])

  const onEnd = (values: any) => {
    console.log(values)
    props.onEditParams && props.onEditParams({
      bloggerActivation, paymentForResult, ...values
    });
  }

  return <div className="balance__container">
    <Descriptions title="Tariff Info" layout="vertical">
      <Descriptions.Item span={1} label="Blogger activation">
        <Text style={{width: "80%"}} editable={editable && {
          autoSize: true,
          onChange: (bloggerActivation) => {
            setBloggerActivation(bloggerActivation);
            onEnd({bloggerActivation});
          }
        }}>{bloggerActivation}</Text>
      </Descriptions.Item>
      <Descriptions.Item span={1} label="Payment for result">
        <Text style={{width: "80%"}} editable={editable && {
          onChange: (paymentForResult) => {
            setPaymentForResult(paymentForResult);
            onEnd({paymentForResult})
          }
        }}>{paymentForResult}</Text>
      </Descriptions.Item>
    </Descriptions>
    <div className="balance__value">
      <span style={{marginRight: "8px"}} className="balance__value-balance">Balance:</span>
      <span style={{marginRight: "8px"}}>{props.balance} </span>
      <span style={{marginRight: "8px"}}>$</span>
      {currentUser?.role === 'ADMIN' ?
        <Button type="primary" onClick={() => props.onReplenish()}><Bold>Pay</Bold></Button> : null}
    </div>
  </div>
}
