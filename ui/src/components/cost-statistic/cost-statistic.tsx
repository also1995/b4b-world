import React from "react";
import "./cost-statistic.less";
import {Divider} from "antd";

export interface CostStatisticProps {
    cost: number;
    leads?: number;
    clicks?: number;
    coverage?: number;
}

export interface CostStatisticItemProps {
    name: string;
    value: string;
}

const CostStatisticItem: React.FC<CostStatisticItemProps> = props => {
  return <div className="cost-statistic__item">
    <div className="cost-statistic__item-title">{props.name}</div>
    <div className="cost-statistic__item-value">{props.value}</div>
  </div>
}

const getValue = (cost: number, val: number | undefined): string => {
  cost = cost || 0;
  return val? (cost / val).toFixed(2) : "0";
}

export const CostStatistic: React.FC<CostStatisticProps> = props => {
  return <div className="cost-statistic__wrapper">
    <Divider orientation="center">Metrics</Divider>
    <div className="cost-statistic__container">
      <CostStatisticItem name="CPA" value={getValue(props.cost, props.leads)} />
      <CostStatisticItem name="CPC" value={getValue(props.cost, props.clicks)} />
      <CostStatisticItem name="CPM" value={getValue(props.cost, props.coverage)} />
    </div>
  </div>
}