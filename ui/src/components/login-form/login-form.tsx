import React, {useContext} from "react";
import {Button, Form, Input} from "antd";
import {LockOutlined, UserOutlined} from "@ant-design/icons";
import "./login-form.less";
import {Link} from "react-router-dom";
import {AuthContext} from "../../contexts/auth.provider";

export const LoginForm: React.FC = () => {
  const {login} = useContext(AuthContext);

  const onFinish = async (values: any) => {
    await login(values);
  };

  return <Form name="login"
    className="login-form__container"
    size="large"
    initialValues={{remember: true}}
    onFinish={onFinish}
  >
    <Form.Item name="email"
      rules={[
        {
          type: 'email',
          message: 'Email is not valid!',
        },
        {
          required: true,
          message: 'Please, enter E-mail!',
        },
      ]}
    >
      <Input prefix={<UserOutlined className="site-form-item-icon"/>} placeholder="Email"/>
    </Form.Item>
    <Form.Item name="password"
      rules={[{required: true, message: 'Please, enter password!'}]}
    >
      <Input prefix={<LockOutlined className="site-form-item-icon"/>}
        type="password"
        placeholder="Password"
      />
    </Form.Item>
    <Form.Item>
      <Button type="primary"
        htmlType="submit"
        className="login-form__form-button">
                Log In
      </Button>
    </Form.Item>
    <div className="login-form__forget">
      <span><Link to="/registration">Register!</Link></span>
    </div>
  </Form>
}
