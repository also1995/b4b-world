import React from "react";
import {Tag} from "antd";

export enum OperationStatus {
    APPROVED = "APPROVED",
    REJECTED = "REJECTED",
    WAITING = "WAITING"
}

export interface StatusProps {
    status: OperationStatus;
}

const OPERATION_STATUS_COLOR_MAP: any = {
  [OperationStatus.APPROVED]: "green",
  [OperationStatus.WAITING]: "#824c97",
  [OperationStatus.REJECTED]: "red"
}

export const OperationStatusTag: React.FC<StatusProps> = props => {
  return <Tag style={{borderRadius: 8, fontWeight: 700}} color={OPERATION_STATUS_COLOR_MAP[props.status]}>{props.status}</Tag>
}