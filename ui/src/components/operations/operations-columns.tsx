import React from "react";
import {getFullDate} from "../../utils/date.util";
import {OperationStatusTag} from "../status/status";

export const operationsTableColumns = [
  {
    title: 'Value',
    dataIndex: 'value'
  },
  {
    title: 'Created At',
    dataIndex: 'createdAt',
    render: (value: string) => getFullDate(value)
  },
  // {
  //   title: 'Approved At',
  //   dataIndex: 'approvedAt',
  //   render: (value: string) => getFullDate(value)
  // },
  {
    title: 'Comment',
    dataIndex: 'comment'
  }
];