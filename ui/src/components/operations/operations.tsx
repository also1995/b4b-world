import React, {useCallback, useEffect, useState} from "react";
import {Form, Input, InputNumber, Modal, Table} from "antd";
import {ApiProvider} from "../../providers/api-provider";
import "./operations.less";
import {operationsTableColumns} from "./operations-columns";
import {Balance} from "../balance/balance";
import {useParams} from "react-router-dom";
import {Action} from "../card-menu/card-menu";
import {ExclamationCircleOutlined} from "@ant-design/icons";
import {DropdownMenu} from "../card-menu/dropdown-menu";
import {User} from "../../models/user";
const { confirm } = Modal;


export const Operations: React.FC<any> = () => {
  const [total, setTotal] = useState<number>(0);
  const [pageSize, setPageSize] = useState<number>(10);
  const [page, setPage] = useState<number>(1);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [data, setData] = useState<any[]>([]);
  const [balanceWindowVisible, setBalanceWindowVisible] = useState<boolean>(false);
  const [currentBusiness, setCurrentBusiness] = useState<any>();

  const [form] = Form.useForm();
  // @ts-ignore
  const {businessId} = useParams();

  const balance = currentBusiness ? currentBusiness.deposit : 0;

  const fetchData = useCallback(async () => {
    const data = await ApiProvider.postRequest<{ data: any[], total: number }>("operations", {
      page,
      count: pageSize,
      filters: [{
        id: 'ID',
        values: [businessId]
      }],
    });
    setData(data.data);
    setTotal(data.total);
    setIsLoading(false);

    const currentBusinessResult = await ApiProvider.getRequest(`businesses/${businessId}`);
    setCurrentBusiness(currentBusinessResult);
  }, [businessId, page, pageSize]);

  useEffect(() => {
    setIsLoading(true)
    fetchData();
  }, [fetchData, page, pageSize]);

  const showWindow = () => {
    setBalanceWindowVisible(true);
  }

  const replenish = async () => {
    const store = await form.validateFields();
    if (store["value"]) {
      await ApiProvider.postRequest("add-deposit", {value: store["value"], comment: store["comment"], businessId});
      setBalanceWindowVisible(false);
      form.resetFields();
      await fetchData();
    }
  }
  
  const changeParams = async (newValues: any) => {
    await ApiProvider.postRequest("users/update-financial-parameters", {...newValues, businessId})
    await fetchData();
  }

  const actionRecordExecute = async (key: Action, record: any) => {
    confirm({
      icon: <ExclamationCircleOutlined />,
      content: <p>Are you sure to remove record ?</p>,
      onOk: async () => {
        await ApiProvider.deleteRequest(`deposit/${record.id}`, null);
        await fetchData();
      }
    })
  }

  const columns = [...operationsTableColumns,
    {
      title: 'Actions',
      render: (value: any, record: any) => {
        return <DropdownMenu items={[{
          key: Action.REMOVE,
          name: "Remove"
        }]} onItemClick={(key) => actionRecordExecute(key, record)}/>
      }
    }
  ]

  return <div>
    <Balance balance={balance}
      paymentForResult={currentBusiness?.paymentForResult}
      bloggerActivation={currentBusiness?.bloggerActivation}
      onEditParams={changeParams}
      onReplenish={showWindow}/>
    <Modal
      title="The deposit will be used to pay for bloggers"
      visible={balanceWindowVisible}
      okText="Pay"
      onOk={() => replenish()}
      onCancel={() => setBalanceWindowVisible(false)}
    >
      <Form form={form} layout="vertical">
        <Form.Item label="Deposit Amount" name="value" rules={[
          {
            required: true,
            message: 'Please, input deposit amount!',
          },
        ]}>
          <InputNumber style={{width: "100%"}} />
        </Form.Item>
        <Form.Item label="Comment" name="comment">
          <Input style={{width: "100%"}} />
        </Form.Item>
      </Form>
    </Modal>
    <Table
      loading={isLoading}
      scroll={{x: 1240, y: "calc(100vh - 328px)"}}
      rowKey="id"
      columns={columns}
      dataSource={data}
      pagination={{
        pageSize,
        total,
        showSizeChanger: true,
        current: page,
        onChange: val => setPage(val),
        onShowSizeChange: (current: number, newSize: number) => {
          setPage(Math.ceil(pageSize * page / newSize))
          setPageSize(newSize);
        }
      }}
    /></div>
}
