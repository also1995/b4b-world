import React from "react";
import "./leads-statistic.less";
import {RADIAN, STATUS_COLOR_MAP} from "../../constants/constants";
import {Cell, Pie, PieChart} from "recharts";
import {ColorLegend} from "../color-legend/color-legend";
import {LeadStatisticItem, LeadStatus} from "../../models/lead";

const renderCustomizedLabel = ({cx, cy, midAngle, innerRadius, outerRadius, percent}: any) => {
  const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy + radius * Math.sin(-midAngle * RADIAN);

  return (
    <text x={x}
      y={y}
      fill="white"
      textAnchor={x > cx ? 'start' : 'end'}
      dominantBaseline="central"
    >
      {`${(percent * 100).toFixed(0)}%`}
    </text>
  );
};


export interface LeadsStatisticProps {
    data: LeadStatisticItem[];
}

export const LeadsStatistic: React.FC<LeadsStatisticProps> = props => {
  const {data} = props;
    
  return <div className="leads-statistic__container">
    <PieChart width={300} height={300} compact>
      <Pie
        data={data}
        cx={120}
        cy={120}
        labelLine={false}
        label={renderCustomizedLabel}
        outerRadius={120}
        fill="#8884d8"
        dataKey="value"
      >
        {
          data.map((entry, index) =>
            <Cell key={`cell-${index}`}
              fill={STATUS_COLOR_MAP[entry.status as LeadStatus]}
            />)
        }
      </Pie>
    </PieChart>
    <ColorLegend title="Leads" items={data.map(item => ({
      name: `${item.status} ${item.value}`,
      color: STATUS_COLOR_MAP[item.status as LeadStatus]
    }))}/>
  </div>
}