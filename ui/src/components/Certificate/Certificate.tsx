import React, {useEffect, useState} from 'react';
import './Certificate.less';
import {Typography, Button, Form, Input, Spin, Space} from "antd";
import {ApiProvider} from "../../providers/api-provider";

const {Title} = Typography;

type Fields = 'phone' | 'name';

const Certificate: React.FC = () => {
  const [userAgreementsLink, setUserAgreementsLink] = useState<string>('');
  const [formId, setFormId] = useState<string>(window.location.pathname.split("/")[2]);
  const [redirectLink, setRedirectLink] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(true);
  const [notFound, setNotFound] = useState<boolean>(false);
  const [formText, setFormText] = useState<string>("");

  useEffect(() => {
    setUserAgreementsLink('https://allright.com/user_agreement_ru.pdf');

    const fetchForm = async () => {
      await ApiProvider.getRequest(`form/${formId}`)
        .then((res: any) => {
          setFormText(res["text"]);
          setRedirectLink(res["link"]);
          setLoading(false);
        })
        .catch(() => {
          setNotFound(true);
          setLoading(false);
        });
    }
    fetchForm();
  }, [formId]);
  
  

  const onClick = async (data: Record<Fields, string>) => {
    await ApiProvider.postRequest("leads/create", {...data, formId});
    window.location.assign(redirectLink);
  }

  return <div className="lead-form-wrapper">
    {loading ? <Spin tip={'Loading...'} spinning={loading}>
    </Spin> :
      notFound? <div>Page not Found</div> :
        <Form onFinish={onClick}>
          <div className="lead-form-wrapper__heading">
            <Title level={1}>
              {formText}
            </Title>
          </div>


          <div className="lead-form-wrapper__inputs">
            <Form.Item
              name={'phone'}
              rules={[
                {
                  required: true,
                  message: 'Укажите, пожалуйста, корректный номер телефона'
                }
              ]}
              className="lead"
            >
              <Input placeholder={'+7(999)999-99-99'} bordered={false} type={'tel'}/>
            </Form.Item>
            <Form.Item
              name={'name'}
              rules={[
                {
                  required: true,
                  message: 'Обязательное поле'
                }
              ]}
            >
              <Input placeholder={'Ваше Имя'} bordered={false}/>
            </Form.Item>
          </div>

          <Space/>

          <div className="lead-form-wrapper__footer">
            <Form.Item
              wrapperCol={{
                span: 24
              }}
              style={{
                textAlign: 'center'
              }}
            >
              <Button type={'primary'} htmlType={'submit'}>
                            Получить и перейти на сайт
              </Button>
            </Form.Item>
            <Form.Item>
                        *Нажимая кнопку, Вы принимаете условия <a href={userAgreementsLink} target={'_blank'}>пользовательского
                        соглашения</a>
            </Form.Item>
          </div>

        </Form>}
  </div>
}


export default Certificate;
