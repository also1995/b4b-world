import React, {useContext, useState} from "react";
import "./leads-table.less";
import {getFullDate} from "../../utils/date.util";
import {AdCompany} from "../../models/ad-company";
import {Lead, LeadStatus} from "../../models/lead";
import {StatusTag} from "../contacts/status";
import {DropdownMenu} from "../card-menu/dropdown-menu";
import {Action} from "../card-menu/card-menu";
import {ApiProvider} from "../../providers/api-provider";
import {Modal, Table} from "antd";
import {UpdateLead} from "../lead/update-lead";
import {AuthContext} from "../../contexts/auth.provider";
import {EditOutlined} from "@ant-design/icons";

export interface LeadsTableProps {
    leads: Lead[];
    onUpdate: Function;
}

export const LeadsTable: React.FC<LeadsTableProps> = props => {
  const {leads} = props;
  const [editable, setEditable] = useState<any>(null);
  const [currentRecord, setCurrentRecord] = useState<any>(null);
  const [editLeadModalIsVisible, setEditLeadModalIsVisible] = useState<boolean>(false);
  const {currentUser} = useContext(AuthContext);


  const columns = [
    {
      title: 'Creation Date',
      dataIndex: 'creationDate',
      minWidth: "25%",
      ellipsis: true,
      render: getFullDate

    },
    {
      title: 'Name',
      dataIndex: 'name',
      ellipsis: true
    },
    {
      title: 'Phone',
      dataIndex: 'phone',
    },
    {
      title: 'Blogger',
      dataIndex: 'blogger',
      render: (value: AdCompany['blogger']) => <a href={value.link} target={'_blank'}>
        <b>{value.nick}</b>
      </a>
    },
    {
      title: 'Status',
      dataIndex: 'status',
      render: (value: LeadStatus) => <StatusTag status={value}/> || "-"
    },
    {
      title: 'B4B Comment',
      dataIndex: 'adminComment',
      render: (value: string, record: any) => <div className="leads-table__comment-column">
        <span className="leads-table__comment-column-value">
          {value}
        </span>
        {currentUser?.role === "ADMIN" ? <EditOutlined className="leads-table__comment-column-button" onClick={() => actionRecordExecute(Action.EDIT, record)} /> : null}
      </div>
    }, {
      title: 'Comment',
      dataIndex: 'comment',
      render: (value: string, record: any) => <div className="leads-table__comment-column">
        <span className="leads-table__comment-column-value">
          {value}
        </span>
        {currentUser?.role !== "ADMIN" ? <EditOutlined className="leads-table__comment-column-button" onClick={() => actionRecordExecute(Action.EDIT, record)} /> : null}
      </div>
    },
    {
      title: 'Actions',
      render: (value: any, record: any) => {
        const items = [{
          key: Action.ACCEPT,
          name: "Accept"
        }, {
          key: Action.IN_PROGRESS,
          name: "In Progress"
        }, {
          key: Action.ERROR,
          name: "Error"
        }, {
          key: Action.RESET,
          name: "Reset"
        }];
        return <DropdownMenu items={[...items, {
          key: Action.EDIT,
          name: "Edit"
        }]} onItemClick={(key) => actionRecordExecute(key, record)}/>
      }
    }
  ];

  const actionRecordExecute = async (action: Action, record: any) => {
    if (action === Action.ERROR) {
      await ApiProvider.postRequest(`leads/${record.id}/error`, null);
      props.onUpdate();
    } else if (action === Action.ACCEPT) {
      await ApiProvider.postRequest(`leads/${record.id}/accept`, null);
      props.onUpdate();
    } else if (action === Action.RESET) {
      await ApiProvider.postRequest(`leads/${record.id}/reset`, null);
      props.onUpdate();
    } else if (action === Action.EDIT) {
      setCurrentRecord(record);

      if (currentUser?.role === "ADMIN") {
        setEditable({
          name: record.name,
          phone: record.phone,
          comment: record.adminComment
        });
      } else {
        setEditable({
          name: record.name,
          phone: record.phone,
          comment: record.comment
        });
      }

      setEditLeadModalIsVisible(true)
    } else if (action === Action.IN_PROGRESS) {
      await ApiProvider.postRequest(`leads/${record.id}/in-progress`, null);
      props.onUpdate();
    }
  }

  const updateRecord = async (value: any) => {
    await ApiProvider.postRequest(`leads/update`, {...value, id: currentRecord.id, type: currentUser?.role});
    props.onUpdate();
    setEditable(null);
    setCurrentRecord(null);
    setEditLeadModalIsVisible(false);
  }


  return <>
    <Table style={{width: "100%"}}
      columns={columns}
      dataSource={leads}
    />
    <Modal className="ad-companies__create-modal"
      visible={editLeadModalIsVisible}
      title="Update Lead"
      okText="Update"
      onCancel={() => {
        setEditLeadModalIsVisible(false);
      }}
      destroyOnClose={true}
    >
      <UpdateLead values={editable} onSubmit={updateRecord}/>
    </Modal>
  </>
}