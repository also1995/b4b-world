import React, {useState} from "react";
import {Button, DatePicker, Form, Input, Modal, Radio, Table} from "antd";
import {IInfluencer} from "../../../models/influencer";
import {ApiProvider} from "../../../providers/api-provider";
import {businessHashconnectService} from "../../../influencer/hashconnect";
import moment from "moment";
import "./influencers-list.less"

interface IProps {
  data: IInfluencer[];
}

const { RangePicker } = DatePicker;

export const InfluencersList: React.FC<IProps> = ({ data }) => {
  const [modalOpened, setModalOpened] = useState<boolean>(false);
  const [influencer, setInfluencer] = useState<IInfluencer>();
  const [form] = Form.useForm();

  const createAd = async (values: any) => {
    await businessHashconnectService.requestBooking(moment(values.date).format('YYYY-MM-DD'), influencer?.price!).catch((err) => console.error(err))
    const data = await ApiProvider.postRequest("ad-companies/create", {...values, bloggerId: influencer?.id});
    onCloseModal();
  }

  const openModal = (influencer: IInfluencer) => {
    setInfluencer(influencer);
    setModalOpened(true);
  }

  const onCloseModal = () => {
    setModalOpened(false);
  }

  const COLUMNS = [
    {
      title: 'ID',
      dataIndex: 'id',
      ellipsis: true,
    },
    {
      title: 'Influencer',
      dataIndex: 'name',
      ellipsis: true,
    },
    {
      title: 'Raiting (B4B points)',
      dataIndex: 'raiting',
      ellipsis: true,
    },
    {
      title: 'Social Media',
      dataIndex: 'socialMedia',
      ellipsis: true,
    },
    {
      title: 'Subscribers',
      dataIndex: 'subscribers',
      ellipsis: true,
    },
    {
      title: 'ER',
      dataIndex: 'er',
      ellipsis: true,
    },
    {
      title: 'Price',
      dataIndex: 'price',
      ellipsis: true,
    },
    {
      title: 'Price Pin',
      dataIndex: 'pricePin',
      ellipsis: true,
    },
    {
      title: '',
      dataIndex: 'id',
      render: (data: string, record: IInfluencer) => <Button onClick={() => openModal(record)}>Book</Button>
    },
  ]

  return <>
    <Modal footer={null} visible={modalOpened} onCancel={() => setModalOpened(false)}>
      <Form onFinish={createAd} form={form} layout="vertical">
        <Form.Item name="date" label="Select date" rules={[
          {
            required: true
          }
        ]}>
          <DatePicker style={{width: "100%"}} />
        </Form.Item>
        <Form.Item name="adFormat" 
          label="Choose the ad format"
          rules={[
            {
              required: true
            }
          ]}>
          <Radio.Group options={[{ value: "PIN", label: `Post - ${influencer?.price}$` }, { value: "POST_AND_PIN", label: `Post+Pin 24h ${influencer?.pricePin}$` }]} />
        </Form.Item>
        <Form.Item name="description" 
          label="Attach a link to the brief"
          rules={[
            {
              required: true
            }
          ]}>
          <Input />
        </Form.Item>
        <div className="influencers-buttons-wrapper">
          <Button htmlType="submit" type="primary">Send a request</Button>
          <Button onClick={onCloseModal}>Cancel</Button>
        </div>
      </Form>
    </Modal>
    <Table columns={COLUMNS} dataSource={data} />
  </>;
}

