import React from "react";
import {Checkbox, Select, DatePicker, Popover, Button, Typography, Form, Radio} from "antd";
import "./influencers-filter.less";

const { RangePicker } = DatePicker;
const { Title } = Typography;

interface IProps {
  onApply: (values: any) => void;
  onCancel: () => void;
}

const SOCIAL_NETWORK_OPTIONS = [
  {
    label: "Twitter",
    value: "TWITTER"
  },
  {
    label: "Telegram",
    value: "TELEGRAM"
  },
  {
    label: "Instagram",
    value: "INSTAGRAM"
  },
  {
    label: "Tik-Tok",
    value: "TIKTOK"
  },
];

const INFLUENCERS_COUNT_OPTIONS = [
  {
    label: "Mini (10-100K)",
    value: "MINI"
  },
  {
    label: "Middle (100-900K)",
    value: "MIDDLE"
  },
  {
    label: "Macro (900K+)",
    value: "MACRO"
  }
]

const INFLUENCERS_CATEGORY = [
  {
    label: "1",
    value: "1"
  },
  {
    label: "2",
    value: "2"
  },{
    label: "3",
    value: "3"
  }

]

export const InfluencersFilter: React.FC<IProps> = ({onApply, onCancel}) => {
  const [form] = Form.useForm();

  const onFinish = (values: any) => {
    console.log('Received values of form: ', values);
    onApply(values);
  };

  const filter = 
    <Form form={form}
      onFinish={onFinish}
      layout="vertical"    
      name="filter"
      className="influencers-filter-wrapper">
      <Form.Item label={<Title level={5}>Social Networks</Title>} name="socialMedia" valuePropName="checked">
        <Checkbox.Group options={SOCIAL_NETWORK_OPTIONS}></Checkbox.Group>
      </Form.Item>
      <Form.Item label={<Title level={5}>Subscribers</Title>} name="subscribersCount">
        <Radio.Group options={INFLUENCERS_COUNT_OPTIONS}></Radio.Group>
      </Form.Item>
      <Form.Item label={<Title level={5}>Category</Title>} name="category">
        <Select options={INFLUENCERS_CATEGORY}></Select>
      </Form.Item>
      <Form.Item label={<Title level={5}>Date Range</Title>} name="dateRange">         
        <RangePicker />
      </Form.Item>
      <Button htmlType="submit" type="primary">Apply</Button>
      <Button onClick={() => form.resetFields()}>Clear</Button>
    </Form>
  
  return <Popover placement="leftTop" trigger="click" content={filter}>
    <Button type="primary">Filter</Button>
  </Popover>
}
