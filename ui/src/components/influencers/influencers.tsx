import React, {useEffect, useState} from "react";
import {InfluencersFilter} from "./filter/influencers-filter";
import {InfluencersList} from "./list/influencers-list";
import {IInfluencer} from "../../models/influencer";
import "./influencers.less";
import {ApiProvider} from "../../providers/api-provider";
import {useHashConnect} from "../../hooks/useHashConnect";
import {businessHashconnectService} from "../../influencer/hashconnect";
import {Collapse, Typography} from "antd";

const { Panel } = Collapse;

export const Influencers: React.FC = () => {
  const [data, setData] = useState<IInfluencer[]>([]);
  const { pairingString, isPaired } = useHashConnect(businessHashconnectService);

  useEffect(() => {
    onApply({});
  }, []);

  const onApply = async (values: any) => {
    const data: any[] = await ApiProvider.postRequest("bloggers", values);
    setData(data);
  }

  const onCancel = () => {
    console.log("cancel");
  }

  return <div>
    <div className="influencers-filter">
      <Typography.Text style={{maxWidth: "200px"}} ellipsis copyable={!isPaired}>{pairingString}</Typography.Text>
      <InfluencersFilter onApply={onApply} onCancel={onCancel} />
    </div>
    <InfluencersList data={data} />
  </div>
}
