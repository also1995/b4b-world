import React, {useContext} from 'react';
import {Button, Form, Input} from "antd";
import {AuthContext} from "../../contexts/auth.provider";


export interface UpdateLeadProps {
  onSubmit: (values: any) => void;
  values: any;
}

export const UpdateLead: React.FC<UpdateLeadProps> = props => {
  const {currentUser} = useContext(AuthContext);
  
  return <>
    <Form
      initialValues={props.values}
      labelCol={{span: 5}}
      // wrapperCol={{span: 14}}
      layout="horizontal"
      onFinish={props.onSubmit}
    >
      <Form.Item
        hidden={currentUser?.role !== "ADMIN"}
        name='name'
        label={'Name'}
        rules={[
          {
            required: true
          }
        ]}
      >
        <Input style={{width: "100%"}} />
      </Form.Item>
      <Form.Item
        name='phone'
        label={'Phone'}
        hidden={currentUser?.role !== "ADMIN"}
        rules={[
          {
            required: true
          }
        ]}
      >
        <Input style={{width: "100%"}}  />
      </Form.Item>
      <Form.Item
        name='comment'
        label={'Comment'}
      >
        <Input.TextArea style={{width: "100%"}}  rows={6} />
      </Form.Item>
      <Form.Item
        wrapperCol={{
          span: 24
        }}
        style={{
          textAlign: 'center'
        }}
      >
        <Button style={{width: "100%"}} htmlType={'submit'} type={'primary'}>
          Save
        </Button>
      </Form.Item>
    </Form>
  </>
}