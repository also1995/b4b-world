import React from 'react';
import {Button, Form, Input} from "antd";
import {ApiProvider} from "../../providers/api-provider";


export interface NewLeadProps {
  onSubmit: Function;
  businessId?: string;
}

const NewLead: React.FC<NewLeadProps> = props => {
  const onSubmit = async (data: any) => {
    await ApiProvider.postRequest(`leads/create/${props.businessId}`, data);
    props.onSubmit();
  }

  return <>
    <Form
      labelCol={{span: 5}}
      wrapperCol={{span: 14}}
      layout="horizontal"
      onFinish={onSubmit}
    >
      <Form.Item
        name='name'
        label={'Name'}
        rules={[
          {
            required: true
          }
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name='phone'
        label={'Phone'}
        rules={[
          {
            required: true
          }
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        wrapperCol={{
          span: 24
        }}
        style={{
          textAlign: 'center'
        }}
        rules={[
          {
            required: true
          }
        ]}
      >
        <Button htmlType={'submit'} type={'primary'}>
          Submit
        </Button>
      </Form.Item>
    </Form>
  </>
}


export default NewLead;
