import React from "react";
import "./statistic.less";
import {Descriptions} from "antd";
import {Typography} from "antd";

export interface StatisticProps {
    coverage: number;
    clicks: number;
    contacts: number;
}

const {Title} = Typography;

export const Statistic: React.FC<StatisticProps> = props => {
  return <div className="statistic__container">
    <Title level={3}>Statistic</Title>
    <Descriptions>
      <Descriptions.Item label="Coverage">{props.coverage}</Descriptions.Item>
      <Descriptions.Item label="Clicks">{props.clicks}</Descriptions.Item>
      <Descriptions.Item label="Leads">{props.contacts}</Descriptions.Item>
    </Descriptions>
  </div>
}