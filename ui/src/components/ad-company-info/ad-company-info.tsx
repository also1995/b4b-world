import React, {useContext, useState} from "react";
import "./ad-company-info.less";
import {Button} from "antd";
import {InfoItem} from "../info-item/info-item";
import {AdCompany} from "../../models/ad-company";
import {AuthContext} from "../../contexts/auth.provider";

export interface AdCompanyInfoPops {
  company: AdCompany;
  onSave: Function;
}

export const AdCompanyInfo: React.FC<AdCompanyInfoPops> = props => {
  const [editable, setEditable] = useState<boolean>(false);
  const {company} = props;
  const [initialValue, setInitialValue] = useState({
    publicationDate: company.publicationDate,
    comment: company.comment,
    coverage: company.result.coverage,
    clicks: company.result.clicks,
    promoCode: company.promoCode,
    site: company.resultLink,
    utmLink: company.utmLink,
    cost: company.cost || 0
  });
  const {currentUser} = useContext(AuthContext);
  const disabled = currentUser?.role !== "ADMIN";

  let values = {...initialValue};

  return <div className="ad-company-info__container">
    <InfoItem name="Promocode" 
      value={company.promoCode}
      editable={editable}
      onChange={(value: any) => {values = {...values, promoCode: value}}}
    />
    <InfoItem name="UTM Link"
      value={company.utmLink}
      editable={editable}
      onChange={(value: any) => {values = {...values, utmLink: value}}}
    />
    <InfoItem name="Publication Date"
      value={new Date(company.publicationDate).toLocaleString()}
      editable={editable}
      disabled={disabled}
      onChange={(value: any) => {values = {...values, publicationDate: value}}}
    />
    <InfoItem name="Comment"
      value={company.comment} 
      editable={editable}
      disabled={disabled}        
      onChange={(value: any) => {values = {...values, comment: value}}}/>
    <InfoItem name="Cost"
      value={company.cost || 0}
      editable={editable}
      disabled={disabled}
      onChange={(value: any) => {values = {...values, cost: value}}}/>
    <InfoItem name="Results"
      value={company.resultLink}
      editable={editable}
      disabled={disabled}
      onChange={(value: any) => {values = {...values, site: value}}}/>
    <InfoItem name="Clicks"
      value={company.result.clicks}
      editable={editable}
      disabled={disabled}
      onChange={(value: any) => {values = {...values, clicks: Number(value)}}}
    />
    <InfoItem name="Coverage" 
      value={company.result.coverage}
      editable={editable}
      disabled={disabled}
      onChange={(value: any) => {values = {...values, coverage: Number(value)}}}
    />
    <div className="ad-company-info__buttons-wrapper">
      {
        editable ?
          <>
            <Button type="primary"
              className="ad-company-info__button"
              onClick={() => {setEditable(false); setInitialValue(values); props.onSave(values)}}>
                            Save
            </Button>
            <Button className="ad-company-info__button" danger style={{marginLeft: "8px"}} onClick={() => {setEditable(false)}}>
                            Cancel
            </Button>
          </> :
          <Button className="ad-company-info__button" onClick={() => setEditable(true)}>
              Edit
          </Button>
      }
    </div>
  </div>
}
