import React from "react";
import {Menu} from "antd";
import "./card-menu.less";

export interface CardMenuItem {
    key: Action;
    name: string;
    isLink?: boolean
}

export enum Action {
    ACCEPT = "ACCEPT",
    ERROR = "ERROR",
    RESET = "RESET",
    EDIT = "EDIT",
    IN_PROGRESS = "IN_PROGRESS",
    REMOVE = "REMOVE"
}

export interface CardMenuProps {
    items: CardMenuItem[];
    onItemClick: (key: Action) => void
}

export const CardMenu: React.FC<CardMenuProps> = props => {
  return <Menu className="card-menu__container">
    {props.items.map(item =>
      <Menu.Item key={item.key}>
        <a onClick={() => props.onItemClick(item.key)}>{item.name}</a>
      </Menu.Item>)}
  </Menu>
}