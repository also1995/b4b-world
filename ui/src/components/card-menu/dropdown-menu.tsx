import React, {useState} from "react";
import {CardMenu, CardMenuProps} from "./card-menu";
import {EllipsisOutlined} from "@ant-design/icons";
import {Dropdown} from "antd";

export type DropdownMenuProps = CardMenuProps;

export const DropdownMenu: React.FC<DropdownMenuProps> = props => {
  const [visible, setVisible] = useState<boolean>(false);

  return <Dropdown placement="bottomRight"
    visible={visible}
    onVisibleChange={setVisible}
    overlay={<CardMenu items={props.items}
      onItemClick={(key) => {
        setVisible(!visible);
        props.onItemClick(key);
      }}/>
    }
    trigger={["click"]}>
    <div className="card__actions-wrapper">
      <EllipsisOutlined className="card__actions"/>
    </div>
  </Dropdown>
}