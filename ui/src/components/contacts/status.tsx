import React from "react";
import {Tag} from "antd";
import {STATUS_COLOR_MAP} from "../../constants/constants";
import {LeadStatus} from "../../models/lead";


export interface StatusProps {
    status: LeadStatus;
}

export const StatusTag: React.FC<StatusProps> = props => {
  return <Tag style={{borderRadius: 8, fontWeight: 700}} color={STATUS_COLOR_MAP[props.status]}>{props.status}</Tag>
}