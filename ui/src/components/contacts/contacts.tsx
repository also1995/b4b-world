import React, {useState} from "react";
import "./contacts.less";
import {Button, Modal} from "antd";
import {useParams} from "react-router-dom";
import {Bold} from "../balance/balance";
import {LeadsTable} from "../leads/leads-table";

export interface ContactsProps {
    businessId: string;
}

export const Contacts: React.FC = props => {
  const [modalVisible, setModalVisible] = useState<boolean>(false);

  const showModal = () => {
    setModalVisible(true);
  }

  const hideModal = () => {
    setModalVisible(false);
  }

  return <div className="contacts__container">
    <Button style={{height: "50px", width: "200px"}} onClick={showModal} type="primary">
      <Bold>Leads</Bold>
    </Button>
    <Modal
      title={'Leads'}
      width={820}
      visible={modalVisible}
      onCancel={hideModal}
      onOk={hideModal}
      destroyOnClose={true}
    >
      <LeadsTable leads={[]} onUpdate={() => {}} />
    </Modal>
  </div>
}
