import React, {useContext} from 'react';
import {Avatar, Button, Layout} from 'antd';
import {Link, useNavigate} from "react-router-dom";
import {AuthContext} from "../../contexts/auth.provider";
import {UserOutlined} from "@ant-design/icons";
import './header.less';
import {Bold} from "../balance/balance";

const {Header} = Layout;

const AppHeader = () => {
  const {logout, isAuthorized, currentUser} = useContext(AuthContext);
  const navigate = useNavigate();

  return (
    <Header className="header">
      <Link className="header__logo" to="/"><img src="/b4b_header.png" style={{marginBottom: 4}} height={64}></img></Link>
      {
        isAuthorized ?
          <div className="header__buttons-wrapper">
            <div className="header__profile-info">
              <Avatar size={48} icon={<UserOutlined/>}/>
              <span className="header__profile-name">
                {currentUser?.entity.name}
              </span>
            </div>
            <Button type="primary" style={{height: "100%"}} onClick={() => logout()}>
              <Bold>Log Out</Bold>
            </Button>
          </div> :
          <div className="header__buttons-wrapper">
            <Button type="primary" style={{height: "100%"}} onClick={() => navigate("/registration")}>
              <Bold>Sign Up</Bold>
            </Button>
            <Button type="primary" style={{height: "100%"}} className="header__sign-in"
              onClick={() => navigate("/login")}>
              <Bold>Sign In</Bold>
            </Button>
          </div>
      }
    </Header>
  );
};

export default AppHeader;
