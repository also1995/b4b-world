import React, {ReactNode, useState} from "react";
import "./info-item.less";
import {Input} from "antd";

export interface InfoItem {
    name: string;
    value: string | number | ReactNode;
    editable?: boolean;
    disabled?: boolean;
    onChange?: Function;
}

export const InfoItem: React.FC<InfoItem> = item => {
  const [value, setValue] = useState(item.value); 

  const onChangeValue = (e: any) => {
    setValue(e.target.value);
    item.onChange && item.onChange(e.target.value);
  }

  return <div className="info-item__container">
    <span className="info-item-title">{item.name}:</span>
    {item.editable ? <Input disabled={item.disabled} value={value as string} onChange={onChangeValue} /> : <span className="info-item__value">{item.value}</span>}
  </div>    
}