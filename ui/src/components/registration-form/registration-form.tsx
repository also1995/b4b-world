import React, {useState} from "react";
import "./registration-form.less";
import {Button, Form, Input, Select} from "antd";
import {ApiProvider} from "../../providers/api-provider";
import {useNavigate} from "react-router-dom";
import {EUserRole} from "../../models/user";

const formItemLayout = {
  labelCol: {
    xs: {span: 24},
    sm: {span: 8},
  },
  wrapperCol: {
    xs: {span: 24},
    sm: {span: 16},
  },
};

export const RegistrationForm: React.FC = () => {
  const navigate = useNavigate();

  const [userType, setUserType] = useState<EUserRole>(EUserRole.BUSINESS)

  const onFinish = async (values: any) => {
    console.log('Received values of form: ', values);
    delete values.confirm;
    if (values.role === EUserRole.BUSINESS) {
      await ApiProvider.postRequest("users/business-registration", values);
    } else {
      await ApiProvider.postRequest("users/blogger-registration", values);
    }
    navigate("/login");
  };

  return <Form name="registration"
    size="large"
    className="login-form__container"
    initialValues={{remember: true, role: EUserRole.BUSINESS}}
    {...formItemLayout}
    onFinish={onFinish}
  >
    <Form.Item name="role" label="User type">
      <Select options={[
        {label: "Influencer", value: EUserRole.BLOGGER},
        {label: "Business", value: EUserRole.BUSINESS}
      ]}
      value={userType}
      defaultValue={EUserRole.BUSINESS}
      onChange={(value => setUserType(value))}/>
    </Form.Item>
    <Form.Item name="email"
      label="Email"
      rules={[
        {
          type: 'email',
          message: 'E-mail is not valid!',
        },
        {
          required: true,
          message: 'Please, input E-mail!',
        },
      ]}
    >
      <Input placeholder="Email"/>
    </Form.Item>
    {
      userType === EUserRole.BUSINESS &&
      <>
        <Form.Item name="site"
          label="Site"
          rules={[{required: true, message: 'Please, input Your site!'}]}
        >
          <Input placeholder="Site"/>
        </Form.Item>
        <Form.Item name="name"
          label="Name"
          rules={[{required: true, message: 'Please, input Your name!'}]}
        >
          <Input placeholder="Name"/>
        </Form.Item>
      </>
    }
    {
      userType === EUserRole.BLOGGER &&
      <>
        <Form.Item name="link" label="link">
          <Input placeholder="Link"/>
        </Form.Item>
        <Form.Item name="name" label="Name">
          <Input placeholder="Name"/>
        </Form.Item>
        <Form.Item name="socialMedia" label="Social Media">
          <Select options={[
            {label: "Twitter", value: "TWITTER"},
            {label: "Telegram", value: "TELEGRAM"},
            {label: "Instagram", value: "INSTAGRAM"},
            {label: "Tik-Tok", value: "TIKTOK"},
          ]}/>
        </Form.Item>
      </>
    }
    <Form.Item name="password"
      label="Password"
      rules={[
        {
          required: true,
          message: 'Please input your password!',
        },
      ]}
      hasFeedback
    >
      <Input.Password/>
    </Form.Item>
    <Form.Item name="confirm"
      dependencies={['password']}
      label="Confirm"
      hasFeedback
      rules={[
        {
          required: true,
          message: 'Please, input password!',
        },
        ({getFieldValue}) => ({
          validator(_, value) {
            if (!value || getFieldValue('password') === value) {
              return Promise.resolve();
            }
            return Promise.reject('Passwords are not the same!!');
          },
        }),
      ]}
    >
      <Input.Password/>
    </Form.Item>
    <Button type="primary"
      htmlType="submit"
      className="registration-form__form-button">
      Submit
    </Button>
  </Form>
}
