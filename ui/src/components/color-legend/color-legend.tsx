import React, {ReactNode} from "react";
import {Typography} from "antd";
import "./color-legend.less";

const {Title} = Typography;

export interface ColorLegendItem {
    color: string;
    name: string | ReactNode;
}

export interface ColorLegendProps {
    title?: string;
    items: ColorLegendItem[];
}

export const ColorLegend: React.FC<ColorLegendProps> = props => {
  return <div className="color-legend__container">
    <Title level={3}>{props.title || "Legend"}</Title>
    <div className="color-legend__items-container">
      {
        props.items.map((item, index) =>
          <div className="color-legend__item" key={index}>
            <span style={{background: item.color}} className="color-legend__item-color"/>
            <span style={{color: item.color}} className="color-legend__item-name">{item.name}</span>
          </div>)
      }
    </div>
  </div>
}