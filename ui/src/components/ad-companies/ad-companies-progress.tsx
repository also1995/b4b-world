import React, {useEffect, useState} from "react";
import {AdCompany, AdCompanyData} from "../../models/ad-company";
import {ApiProvider} from "../../providers/api-provider";
import {Button, Table} from "antd";
import {businessHashconnectService} from "../../influencer/hashconnect";
import moment from "moment";

interface Props {
 businessId?: string;
}

export const AdCompaniesProgress: React.FC<Props> = ({businessId}) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [id, setId] = useState<string | undefined>(undefined);

  const [data, setData] = useState<AdCompany[]>([]);

  const onApprove = async (record: any) => {
    setId(record.id);
    setIsLoading(true);
    console.log(record);
    await businessHashconnectService.approveBooking(moment(record.date).format('YYYY-MM-DD')).catch(console.error);
    await ApiProvider.postRequest(`ad-companies/${record.id}/approve`, null);
    setId(undefined);
    setIsLoading(false);
    await fetch()
  }

  const COLUMNS = [
    {
      title: 'Influencer',
      // width: '25%',
      dataIndex: 'blogger',
      render: (value: AdCompany['blogger']) => <a href={value.link} target={'_blank'}>
        <b>{value.nick}</b>
      </a>
    },
    {
      title: 'Publication date',
      dataIndex: 'publicationDate',
      width: 150,
      render: (value: string) => value ? new Date(value).toLocaleDateString() : 'Not published yet'
    },
    {
      title: 'Price',
      dataIndex: 'cost',
      render: (value: string) => value ? `${value}$` : 0
    },
    {
      title: 'Ad Format',
      dataIndex: 'adFormat',
      render: (value: string) => value === 'POST_AND_PIN' ? 'Post + Pin': 'Post'
    },
    {
      title: 'Status',
      dataIndex: 'status',
      colSpan: 0,
    },
    {
      title: 'Results',
      dataIndex: 'resultLink',
      render: (value: string) => <a href={value} target={'_blank'}  >Link</a>
    },
    {
      title: '',
      dataIndex: 'id',
      render: (value: string, record: AdCompany) => <Button loading={value === id ? isLoading : false} onClick={() => onApprove(record)}>Approve</Button>
    }
  ];

  const fetch = async () => {
    const data: AdCompany[] = await ApiProvider.postRequest(`ad-companies/in-progress`, {businessId});
    setData(data);
  }


  useEffect(() => {
    fetch();
  }, [businessId]);

  return <Table columns={COLUMNS} dataSource={data}></Table>
}
