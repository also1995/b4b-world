import React, {useCallback, useContext, useEffect, useState} from "react";
import "./ad-companies.less";
import {AdCompany, AdCompanyData} from "../../models/ad-company";
import {Statistic} from "../statistic/statistic";
import {Contacts} from "../contacts/contacts";
import {ApiProvider} from "../../providers/api-provider";
import {Button, Modal, Typography} from "antd";
import EditableTable from "./editable-table";
import NewAdCompany from "./new-ad-company";
import {AuthContext} from "../../contexts/auth.provider";

const {Title} = Typography;

interface IAdCompanies {
  businessId?: string;
}

export const AdCompanies: React.FC<IAdCompanies> = ({businessId}) => {
  const [clicks, setClicks] = useState<number>(0);
  const [coverage, setCoverage] = useState<number>(0);
  const [contacts, setContacts] = useState<number>(0);
  const [adCompanies, setAdCompanies] = useState<AdCompany[]>([]);
  const [createAdCompanyVisible, setCreateAdCompanyVisible] = useState<boolean>(false);
  const {currentUser} = useContext(AuthContext);

  const updateData = (adCompanies: AdCompanyData) => {
    setClicks(adCompanies.clicks);
    setCoverage(adCompanies.coverage);
    setContacts(adCompanies.leads);
    setAdCompanies(adCompanies.data);
  }

  const fetchVideos = useCallback(async () => {
    const adCompanies = await ApiProvider.postRequest<AdCompanyData>("ad-companies", {
      businessId
    });
    updateData(adCompanies);
  }, [businessId]);

  const onCompanyCreated = async () => {
    setCreateAdCompanyVisible(false);
    await fetchVideos();
  }

  useEffect(() => {
    const initVideos = async () => {
      await fetchVideos()
    }
    initVideos();
  }, [fetchVideos])

  return <div className="ad-companies__wrapper">
    <Modal className="ad-companies__create-modal"
      width={820}
      visible={createAdCompanyVisible}
      title="Create Ad Company"
      okText="Create"
      onCancel={() => {
        setCreateAdCompanyVisible(false)
      }}
    >
      <NewAdCompany onSubmit={onCompanyCreated} />
    </Modal>
    <div className="ad-companies__toolbar">
      <Statistic coverage={coverage} clicks={clicks} contacts={contacts}/>
      {/*<Contacts/>*/}
    </div>
    <div className="ad-companies__container">
      {adCompanies.length > 0 ?
        <EditableTable adCompanies={adCompanies} fetchVideos={fetchVideos} updateData={updateData}/>
        :
        <div className="ad-companies__no-content">
          <img width="100%" src="/undraw_season.svg"/>
          <Title className="ad-companies__no-content-title" level={2}>No publication yet!</Title>
        </div>
      }
    </div>
  </div>
}
