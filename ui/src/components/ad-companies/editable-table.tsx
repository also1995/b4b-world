import React, {useContext, useState} from 'react';
import {Table, Input, InputNumber, Form, Modal, Tooltip} from 'antd';
import {AdCompany, AdCompanyData} from "../../models/ad-company";
import {ApiProvider} from "../../providers/api-provider";
import {AuthContext} from "../../contexts/auth.provider";
import NewLead from "../lead/new-lead";
import {Link} from "react-router-dom";

interface Item extends AdCompany {}

interface EditableCellProps extends React.HTMLAttributes<HTMLElement> {
	editing: boolean;
	dataIndex: string;
	title: any;
	inputType: 'number' | 'text';
	record: Item;
	index: number;
	children: React.ReactNode;
}

const EditableCell: React.FC<EditableCellProps> = ({editing,
  dataIndex,
  title, inputType,
  record,
  index,
  children,
  ...restProps
}) => {
  const inputNode = inputType === 'number' ? <InputNumber /> : <Input />;

  return (
    <td {...restProps}>
      {editing ? (
        <Form.Item
          name={dataIndex}
          style={{ margin: 0 }}
        >
          {inputNode}
        </Form.Item>
      ) : (
        children
      )}
    </td>
  );
};

interface IEditableTable {
	adCompanies: AdCompany[];
    fetchVideos: () => void;
    updateData: (data: AdCompanyData) => void;
}

const EditableTable: React.FC<IEditableTable> = ({adCompanies: data, fetchVideos, updateData}) => {
  const [form] = Form.useForm<Omit<Item, 'blogger'> & { blogger: string}>();
  const [editingKey, setEditingKey] = useState('');
  const {currentUser} = useContext(AuthContext);
  const [addLeadModalIsVisible, setAddLeadModalIsVisible] = useState<boolean>(false);
  const [currentRecord, setCurrentRecord] = useState<any>(null);

  const isEditing = (record: Item) => record.id === editingKey;

  const edit = (record: Item) => {
    form.setFieldsValue({
      blogger: record.blogger.nick, // а как же href блоггера?
      publicationDate: record.publicationDate,
      result: record.result,
      promoCode: record.promoCode,
      link: record.link,
      comment: record.comment,
    });
    setEditingKey(record.id);
  };

  const cancel = () => {
    setEditingKey('');
  };

  const remove = async (adCompanyId: string) => {
    await ApiProvider.deleteRequest(`ad-companies/${adCompanyId}`);
    await fetchVideos();
  }

  // const save = async (key: React.Key) => {
  //   try {
  //     const row = (await form.validateFields()) as AdCompany;
  //
  //     const newData = [...data];
  //     const index = newData.findIndex(item => key === item.id);
  //     if (index > -1) {
  //       const item = newData[index];
  //       newData.splice(index, 1, {
  //         ...item,
  //         ...row,
  //       });
  //       const newEntry = newData[index];
  //       await ApiProvider.postRequest<AdCompanyData>(`ad-companies/edit`, {
  //         coverage: newEntry.result.coverage,
  //         clicks: newEntry.result.clicks,
  //         ...newEntry
  //       });
  //       fetchVideos();
  //       // updateData(result);
  //       setEditingKey('');
  //     } else {
  //       newData.push(row);
  //       // setData(newData);
  //       setEditingKey('');
  //     }
  //   } catch (errInfo) {
  //     console.log('Validate Failed:', errInfo);
  //   }
  // };

  // const operationsColumn = {
  //   title: 'operation',
  //   dataIndex: 'id',
  //   render: (_: any, record: Item) => {
  //     const editable = isEditing(record);
  //     return editable ? (
  //       <span>
  //         <a onClick={() => save(record.id)} style={{ marginRight: 8 }}>
  //             Save
  //         </a>
  //         <Popconfirm title="Sure to cancel?" onConfirm={cancel}>
  //           <a>Cancel</a>
  //         </Popconfirm>
  //       </span>
  //     ) : (<>
  //       <Typography.Link disabled={editingKey !== ''} onClick={() => edit(record)}>
  //             Edit
  //       </Typography.Link>
  //           &nbsp;|&nbsp;
  //       <Popconfirm title={'Sure to delete?'} onConfirm={() => remove(record.id)} onCancel={cancel}>
  //         <a>Delete</a>
  //       </Popconfirm>
  //        &nbsp;|&nbsp;
  //       <Typography.Link onClick={() => {setCurrentRecord(record); setAddLeadModalIsVisible(true);}}>
  //         <a>Add Lead</a>
  //       </Typography.Link>
  //     </>);
  //   },
  // };

  let columns = [
    {
      title: 'Name',
      dataIndex: 'id',
      ellipsis: true,
      render: (value: string, record: any) => <Link to={`/company/${value}`}>{record.name}</Link>
    },
    {
      title: 'Influencer',
      // width: '25%',
      dataIndex: 'blogger',
      render: (value: AdCompany['blogger']) => <a href={value.link} target={'_blank'}>
        <b>{value.nick}</b>
      </a>
    },
    {
      title: 'Publication date',
      dataIndex: 'publicationDate',
      width: 150,
      editable: true,
      render: (value: string) => value ? new Date(value).toLocaleDateString() : 'Not published yet'
    },
    {
      title: 'Price',
      dataIndex: 'cost',
      ellipsis: true,
      render: (value: string) => value || 0
    },
    {
      title: 'Coverage',
      dataIndex: ['result', 'coverage'],
      editable: true,
      colSpan: 0,
    },
    {
      title: 'Clicks',
      dataIndex: ['result', 'clicks'],
      editable: true,
      colSpan: 0,
    },
    {
      title: 'Leads',
      dataIndex: ['result', 'leads'],
      colSpan: 0,
    },
    {
      title: <>
				Statistic
        <br/>
        <span style={{fontWeight: 'normal'}}>(Coverage&nbsp;|&nbsp;Clicks&nbsp;|&nbsp;Leads)</span>
      </>,
      colSpan: 3,
      dataIndex: 'result',
      // @ts-ignore
      render: (value) => {
        const obj = {
          children: value,
          props: {
          	rowSpan: 1,
            colSpan: 1,
          },
        };
        obj.props.colSpan = 0;
        return obj;
      }
    },
    {
      title: 'Promocode',
      dataIndex: 'promoCode',
      ellipsis: true,
      render: (value: string) => <b>
        {value}
      </b>
    },
    {
      title: 'UTM Link',
      dataIndex: 'utmLink',
      render: (value: AdCompany['utmLink']) => value ? <a href={value} target={'_blank'}>
        Link
      </a> : ""
    },
    {
      title: 'Results',
      dataIndex: 'resultLink',
      editable: true,
      render: (value: string) => <a href={value} target={'_blank'}  >Link</a>
    },
  ];

  // if (currentUser?.role === 'ADMIN') {
  //   columns.push(operationsColumn as any);
  // }

  const mergedColumns = columns.map(col => {
    if (!col.editable) {
      return col;
    }
    return {
      ...col,
      onCell: (record: Item) => ({
        record,
        inputType: col.dataIndex === 'age' ? 'number' : 'text',
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    };
  });

  const leadCreated = async () => {
    setCurrentRecord(null);
    setAddLeadModalIsVisible(false);
    await fetchVideos();
  }

  return (
    <Form form={form} component={false}>
      <Table
        components={{
          body: {
            cell: EditableCell,
          },
        }}
        bordered
        dataSource={data}
        columns={mergedColumns}
        rowClassName="editable-row"
        pagination={{
          onChange: cancel,
        }}
      />
      <Modal className="ad-companies__create-modal"
        visible={addLeadModalIsVisible}
        title="Create Lead"
        okText="Create"
        onCancel={() => {
          setAddLeadModalIsVisible(false);
          setCurrentRecord(null);
        }}
      >
        <NewLead onSubmit={leadCreated} businessId={currentRecord ? currentRecord!.id : null} />
      </Modal>
    </Form>
  );
};


export default EditableTable;
