import React, {useEffect, useState} from 'react';
import {Button, DatePicker, Form, Input, Select, Typography} from "antd";
import {ApiProvider} from "../../providers/api-provider";

const {Title} = Typography;

export interface NewVideoProps {
  onSubmit: Function;
}

const NewAdCompany: React.FC<NewVideoProps> = props => {
  const onSubmit = async (data: any) => {
    await ApiProvider.postRequest("ad-companies/create", data);
    props.onSubmit();
  }
  const [users, setUsers] = useState([]);

  useEffect(() => {
    const fetchUsers = async () => {
      const users = await ApiProvider.getRequest<any []>("businesses");
      setUsers((users as any).map((item: any) => ({
        label: item.name,
        value: item.id
      })));
    }

    fetchUsers();
  }, [])

  return <>
    <Title level={2} style={{textAlign: 'center'}}>
            New Ad Company
    </Title>
    <Form
      labelCol={{span: 5}}
      wrapperCol={{span: 14}}
      layout="horizontal"
      onFinish={onSubmit}
    >
      <Form.Item
        name='name'
        label={'Name'}
        rules={[
          {
            required: true
          }
        ]}
      >
        <Input placeholder={'https://my.company.com'}/>
      </Form.Item>
      <Form.Item
        name='user'
        label={'User'}
        rules={[
          {
            required: true
          }
        ]}
      >
        <Select options={users}/>
      </Form.Item>
      <Form.Item
        name='nick'
        label={'Blogger\'s nickname'}
        rules={[
          {
            required: true
          }
        ]}
      >
        <Input placeholder={'@blogger.nickname'}/>
      </Form.Item>
      <Form.Item
        name={'bloggerLink'}
        label={'Link to blogger\'s page'}
        rules={[
          {
            required: true
          }
        ]}
      >
        <Input placeholder={'https://instagram.com/blogger.nickname'}/>
      </Form.Item>
      <Form.Item
        name={'site'}
        label={'Link to site'}
        rules={[
          {
            required: true
          }
        ]}
      >
        <Input/>
      </Form.Item>
      <Form.Item
        name={'publicationDate'}
        label={'Publication date'}
        rules={[
          {
            required: true
          }
        ]}
      >
        <DatePicker/>
      </Form.Item>
      <Form.Item
        name={'comment'}
        label={'Comment'}
      >
        <Input.TextArea placeholder={'Lessons will start at 25 Sep 10:00 AM (MSK)'}/>
      </Form.Item>
      <Form.Item
        name={'promoCode'}
        label={'Promocode'}
        rules={[
          {
            required: true
          }
        ]}
      >
        <Input placeholder={'MYPROMOCODE'}/>
      </Form.Item>
      <Form.Item
        name={'resultLink'}
        label={'Link to result'}
        rules={[
          {
            required: true
          }
        ]}
      >
        <Input placeholder={'https://www.instagram.com/stories/blogger.nickname/518326745827345872/'}/>
      </Form.Item>
      <Form.Item
        label={'Form Text'}
        name={'formText'}
      >
        <Input.TextArea placeholder={''}/>
      </Form.Item>
      <Form.Item
        wrapperCol={{
          span: 24
        }}
        style={{
          textAlign: 'center'
        }}
        rules={[
          {
            required: true
          }
        ]}
      >
        <Button htmlType={'submit'} type={'primary'}>
                    Add new Ad Company
        </Button>
      </Form.Item>
    </Form>
  </>
}


export default NewAdCompany;
