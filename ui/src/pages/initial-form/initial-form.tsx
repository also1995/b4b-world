import React, {useState} from "react";
import "./initial-form.less";
import {Button, Form, Input, Result, Steps, Upload} from "antd";
import {Typography} from "antd";
import {DeleteOutlined, FileExcelOutlined, InboxOutlined} from "@ant-design/icons";
import { useNavigate } from "react-router-dom";

const {Step} = Steps;
const {Title, Text} = Typography;
const {Dragger} = Upload;

const steps = [
  {
    title: 'Google Form',
    content: 'First-content',
    contentTitle : "To get started, tell us more about your business and the product for which you need to select bloggers "
  },
  {
    title: 'Requisites',
    content: 'Second-content',
    contentTitle: "Company details for the contract"
  },
  {
    title: 'Final',
    content: 'Last-content',
  },
];

export const InitialFormPage: React.FC = () => {
  const [current, setCurrent] = useState<number>(0);
  const [loading, setIsLoading] = useState<boolean>(false);
  const [file, setFile] = useState<Blob | null>(null);
  const navigate = useNavigate();


  const next = () => {
    setCurrent(current + 1);
  };

  const prev = () => {
    setCurrent(current - 1);
  };
  
  const getTitle = () => {
    return steps[current].contentTitle
  }
  
  const getContent = () => {
    switch (current) {
    case 0:
      return <div style={{flex: "1"}}>
        <iframe
          scrolling="no" 
          src="https://docs.google.com/forms/d/e/1FAIpQLSdxMYM_ho79UCygq9ADT98viANtUNcaDgeJpqLsDB9-XstKEw/viewform?embedded=true"
          width="100%" height="1329" frameBorder={0} marginHeight={0} marginWidth={0}>Loading…
        </iframe></div>
    case 1:
      return <Form layout="vertical">
        <Form.Item>
          <Input.TextArea placeholder="Company Requisites" rows={4} />
        </Form.Item>
        {file ? (
          <div className="initial-form__file-uploaded">
            <div className="initial-form__upload-drag-icon">
              <FileExcelOutlined/>
            </div>
            <div className="initial-form__file-name-container">
              <Title className="initial-form__file-name" level={4}>
                {file ? (file as any).name : ""}
              </Title>
              <DeleteOutlined className="initial-form__remove-button"
                onClick={() => {
                  setFile(null)
                }}
              />
            </div>
          </div>
        ) : (
          <Dragger showUploadList={false}
            fileList={file ? [file as any] : []}
            beforeUpload={() => {
              return false;
            }}
            onChange={e => {
              setFile(e.file as any)
            }}
          >
            <div className="initial-form__upload-drag-icon">
              <InboxOutlined height={64} width={64}/>
            </div>
            <Title level={4}>File</Title>
            <Text>
                Add company card
            </Text>
          </Dragger>
        )}
      </Form>
    default:
      return <Result
        status="success"
        title="You will receive a letter confirming the terms of cooperation within 24 hours!"
        subTitle="After signing the contract and making a deposit, you can track the status of placing advertisements in your personal account"
        extra={[
          <Button type="primary" key="console" onClick={() => navigate("/")}>
        Go to account
          </Button>
        ]}
      /> 
    }
  }

  return <div className="initial-form__content">
    <div className="initial-form__container">
      <Steps current={current}>
        {steps.map(item => (
          <Step key={item.title} title={item.title}/>
        ))}
      </Steps>
      <div className="initial-form__form-content">
        <div className="initial-form__form-content-title">
          <Title level={3}>{getTitle()}</Title>
        </div>  
        {getContent()}
      </div>
      <div className="initial-form__buttons">
        {current < steps.length - 1 && (
          <Button type="primary" onClick={() => next()}>
                    Next
          </Button>
        )}
        {current === steps.length - 1 && (
          <Button type="primary">
                    Done
          </Button>
        )}
        {current > 0 && (
          <Button style={{margin: '0 8px'}} onClick={() => prev()}>
                    Previous
          </Button>
        )}
      </div>
    </div>
  </div>
}
