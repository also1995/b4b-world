import React from "react";
import "./registration-page.less";
import {RegistrationForm} from "../../components/registration-form/registration-form";

export const RegistrationPage: React.FC = () => {
  return <div className="registration-page__container">
    <img className="registration-page__image" src="b4b_form.png"/>
    <RegistrationForm />
  </div>
}