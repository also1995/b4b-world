import React, {useEffect, useState} from 'react';
import {Modal, Table, Tabs} from "antd";
import {TabModel} from "../../models/tab";
import {Route, Link, Routes, useNavigate, useLocation} from "react-router-dom";
import useSWR, {mutate} from "swr";
import {ApiProvider} from "../../providers/api-provider";
import './EntryMain.less';
import {Action} from "../../components/card-menu/card-menu";
import {DropdownMenu} from "../../components/card-menu/dropdown-menu";
import {ExclamationCircleOutlined} from "@ant-design/icons";

const {TabPane} = Tabs;

const { confirm } = Modal;

const mainEntryTabs: TabModel[] = [
  {
    name: "Businesses",
    key: "business"
  },
];

interface Item {
  id: string;
  name: string;
  deposit: number;
  site: string;
}

const businessColumns = [
  {
    title: 'Name',
    dataIndex: 'id',
    key: 'id',
    render: (value: string, record: Item) => <Link to={`/business/${record.id}/results`}>
      {record.name}
    </Link>
  },
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Site',
    dataIndex: 'site',
    key: 'site',
    render: (value: string) => <a href={value} target={'_blank'}>
      {value}
    </a>
  },
  {
    title: 'Deposit',
    dataIndex: 'deposit',
    key: 'deposit'
  }
]

const updateActiveTabByLocation = (location: any) => {
  const name = location.pathname.split("/")[1];
  if (name === "finances") return name;
  return "results";
}

const getBusinessURL = `businesses`;

const EntryMain: React.FC = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const [activeTab, setActiveTab] = useState(updateActiveTabByLocation(location));

  const {data} = useSWR<Item[]>(getBusinessURL, (url: string) => ApiProvider.getRequest<Item[]>(url));

  useEffect(() => {
    setActiveTab(updateActiveTabByLocation(location));
  }, [location]);

  useEffect(() => {
    mutate(getBusinessURL);
  }, [activeTab]);

  const actionRecordExecute = async (key: Action, record: any) => {
    confirm({
      icon: <ExclamationCircleOutlined />,
      content: <p>Are you sure to remove record ?</p>,
      onOk: async () => {
        await ApiProvider.deleteRequest(`businesses/${record.id}`, null);
        mutate(getBusinessURL);
      }
    })
  }

  const columns = [...businessColumns,
    {
      title: 'Actions',
      render: (value: any, record: any) => {
        return <DropdownMenu items={[{
          key: Action.REMOVE,
          name: "Remove"
        }]} onItemClick={(key) => actionRecordExecute(key, record)}/>
      }
    }
  ]

  return <div className="entry-main__container">
    <Tabs size="large" activeKey={activeTab} onTabClick={(activeKey: string) => navigate(activeKey)}>
      {mainEntryTabs.map(tab => <TabPane
        tab={<span>{tab.name}</span>}
        key={tab.key}
        forceRender
        disabled={tab.key === 'bloggers'}
      >
      </TabPane>)}
    </Tabs>
    <Routes>
      <Route path="/" element={<Table
        columns={columns}
        dataSource={data}
        loading={!data}
      />}>
      </Route>
    </Routes>
  </div>
}


export default EntryMain;
