import React, {ReactNode, useContext, useEffect, useState} from "react";
import {Avatar, Divider, List, Typography, Comment} from "antd";
import {UserOutlined} from "@ant-design/icons";
import {AuthContext} from "../../contexts/auth.provider";
import "./profile-page.less";
import 'react-calendar-heatmap/dist/styles.css';
import CalendarHeatmap from 'react-calendar-heatmap';

import ReactTooltip from 'react-tooltip';
import {useAPI} from "../../hooks/use-api";


const {Title} = Typography;

interface ProfileItemProps {
    title: string;
    content: string | ReactNode;
}

interface Activity {
    date: Date;
    count: number;
}


const ProfileItem: React.FC<ProfileItemProps> = ({title, content}) => (
  <div className="profile-page__item-profile-wrapper">
    <p className="profile-page__item-profile-p-label">{title}:</p>
    {content}
  </div>
);

const data = [
  {
    actions: [<span key="comment-list-reply-to-0">Reply to</span>],
    author: 'Han Solo',
    avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
    content: (
      <p>
                We supply a series of design principles, practical patterns and high quality design
                resources (Sketch and Axure), to help people create their product prototypes beautifully and
                efficiently.
      </p>
    )
  },
  {
    actions: [<span key="comment-list-reply-to-0">Reply to</span>],
    author: 'Han Solo',
    avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
    content: (
      <p>
                We supply a series of design principles, practical patterns and high quality design
                resources (Sketch and Axure), to help people create their product prototypes beautifully and
                efficiently.
      </p>
    )
  },
];

const getTooltipText = (value: any): string => {
  return value.date ? `${value.date.toLocaleDateString()}: ${value.count} activities` : "No activity this date"
}

export const ProfilePage: React.FC = () => {
  const {currentUser} = useContext(AuthContext);
  const today = new Date();
  const startDate = (new Date()).setDate(today.getDate() - 365);
  const [activities, setActivities] = useState<Activity[]>([]);


  useEffect(() => {
    const fetchActivity = async () => {
      // const data = await activity();
      // setActivities(data.map(item => ({date: new Date(item.date), count: item.count})));
    }
    fetchActivity();
  }, []);


  useEffect(() => {
    // const fetchOperations = async () => {
    //   const operations = await getOperations();
    // }
  }, []);

  return <div className="profile-page__container">
    <div className="profile-page__content">
      <>
        <Avatar size={120} icon={<UserOutlined/>}/>
        <div className="profile-page__profile-info">
          <Title level={3}>{currentUser?.entity.name}</Title>
        </div>
        <div className="profile-page__heatmap-wrapper">
          <CalendarHeatmap
            startDate={startDate}
            endDate={today}
            values={activities}
            tooltipDataAttrs={(value: any) => {
              return {
                'data-tip': getTooltipText(value),
              };
            }}
          />
          <ReactTooltip/>
        </div>
      </>
      <div className="profile-page__activity-info">
        <div className="profile-page__activity-info-item">
          <Divider orientation="left">Activity</Divider>
          <div className="profile-page__list-wrapper">
            <List
              className="comment-list"
              itemLayout="horizontal"
              dataSource={data}
              renderItem={(item: any) => (
                <li>
                  <Comment
                    actions={item.actions}
                    author={currentUser?.entity.name}
                    avatar={item.avatar}
                    content={item.content}
                  />
                </li>
              )}
            />
          </div>
        </div>
        <div className="profile-page__activity-info-item">
          <Divider orientation="left">Problems</Divider>
          <div className="profile-page__list-wrapper">
            <List
              className="comment-list"
              itemLayout="horizontal"
              dataSource={data}
              renderItem={(item: any) => (
                <li>
                  <Comment
                    actions={item.actions}
                    author={currentUser?.entity.name}
                    avatar={item.avatar}
                    content={item.content}
                  />
                </li>
              )}
            />
          </div>
        </div>
      </div>
    </div>
  </div>
}
