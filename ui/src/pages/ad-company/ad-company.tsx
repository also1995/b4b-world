import React, {useContext, useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import {WidgetContainer} from "../../containers/widget/widget-container";
import "./ad-company.less"
import {AdCompanyInfo} from "../../components/ad-company-info/ad-company-info";
import {AdCompany as AdCompanyModel, AdCompanyData} from "../../models/ad-company";
import {ApiProvider} from "../../providers/api-provider";
import {LeadsStatistic} from "../../components/leads-statistic/leads-statistic";
import {Lead} from "../../models/lead";
import {LeadsTable} from "../../components/leads/leads-table";
import {Button, Modal} from "antd";
import NewLead from "../../components/lead/new-lead";
import {AuthContext} from "../../contexts/auth.provider";
import {CostStatistic} from "../../components/cost-statistic/cost-statistic";

export const AdCompany: React.FC = () => {
  const {companyId} = useParams<{ companyId: string }>();
  const [adCompany, setAdCompany] = useState<AdCompanyModel | null>(null);
  const [leads, setLeads] = useState<Lead[]>([]);
  const [addLeadModalIsVisible, setAddLeadModalIsVisible] = useState<boolean>(false);
  const {currentUser} = useContext(AuthContext);


  useEffect(() => {
    const fetchCompany = async () => {
      const company = await ApiProvider.getRequest<AdCompanyModel>(`ad-companies/${companyId}`);
      setAdCompany(company);
    }

    fetchCompany();
  }, [companyId]);

  useEffect(() => {
    const fetchLeads = async () => {
      const leads = await ApiProvider.getRequest<Lead[]>(`leads/${companyId}`);
      setLeads(leads);
    }

    fetchLeads();
  },[companyId]);

  const onLeadCreated = async () => {
    setAddLeadModalIsVisible(false);
    const company = await ApiProvider.getRequest<AdCompanyModel>(`ad-companies/${companyId}`);
    setAdCompany(company);
    const leads = await ApiProvider.getRequest<Lead[]>(`leads/${companyId}`);
    setLeads(leads);
  }
  
  const saveCompany = async (values: any) => {
    await ApiProvider.postRequest<AdCompanyData>(`ad-companies/edit`, {
      ...values, id: companyId
    });
    const company = await ApiProvider.getRequest<AdCompanyModel>(`ad-companies/${companyId}`);
    setAdCompany(company);
  }


  return <div className="ad-company__container">
    <div className="ad-company__container-item _widget">
      <WidgetContainer title="Ad Company">
        {adCompany ? <AdCompanyInfo company={adCompany} onSave={saveCompany} /> : null}
      </WidgetContainer>
      <WidgetContainer title="Statistic">
        <div>
          {adCompany && adCompany.leadsStatistic ? <LeadsStatistic data={adCompany.leadsStatistic}/> : null}
        </div>
        {
            currentUser?.role === "ADMIN" ?
              <Button onClick={() => {
                setAddLeadModalIsVisible(true)
              }} style={{width: "100%"}} type="primary">
                        Add Lead
              </Button> : null
        }
      </WidgetContainer>
    </div>
    <div className="ad-company__container-item">
      {
        adCompany ?
          <CostStatistic cost={adCompany?.cost || 0} {...adCompany.result} /> :
          null
      }
    </div>
    <div className="ad-company__container-item"> 
      <LeadsTable leads={leads} onUpdate={onLeadCreated}/>
    </div>
    <Modal className="ad-companies__create-modal"
      visible={addLeadModalIsVisible}
      title="Create Lead"
      okText="Create"
      onCancel={() => {
        setAddLeadModalIsVisible(false);
      }}
      destroyOnClose={true}
    >
      <NewLead onSubmit={onLeadCreated} businessId={companyId}/>
    </Modal>
  </div>
}
