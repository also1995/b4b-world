import React, {useContext, useEffect, useState} from 'react';
import {Layout, Tabs} from 'antd';
import {Route, useNavigate, useParams, useLocation, Routes,} from 'react-router-dom';
import {TabModel} from "../../models/tab";
import './main-page.less';
import {AdCompanies} from "../../components/ad-companies/ad-companies";
import {Operations} from "../../components/operations/operations";
import {Influencers} from "../../components/influencers/influencers";
import {AuthContext} from "../../contexts/auth.provider";
import {AdCompaniesProgress} from "../../components/ad-companies/ad-companies-progress";

const {Content} = Layout;
const {TabPane} = Tabs;

const TABS: TabModel[] = [
  {
    name: "Influencers list",
    key: "influencers"
  },
  {
    name: "Ads in progress",
    key: "in-progress"
  },
  {
    name: "Ads results",
    key: "results"
  },
  {
    name: "Finances",
    key: "finances"
  }
];


const updateActiveTabByLocation = (location: any) => {
  const name = location.pathname;
  console.log(name)
  if (name.includes("finances")) return 'finances';
  if (name.includes("results")) return 'results';
  if (name.includes("in-progress")) return 'in-progress';
  return "influencers";
}

export const MainPage: React.FC = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const [activeTab, setActiveTab] = useState(updateActiveTabByLocation(location));
  const {businessId} = useParams<{businessId: string}>();
  const {currentUser} = useContext(AuthContext);


  useEffect(() => {
    setActiveTab(updateActiveTabByLocation(location));
  }, [location]);


  return (
    <Content className="main-page__content-container">
      <Tabs size="large" activeKey={activeTab} onTabClick={(activeKey: string) => navigate(activeKey)}>
        {TABS.filter(item => currentUser?.role === "ADMIN" ? !["influencers", "in-progress"].includes(item.key) : true).map(tab => <TabPane
          tab={<span>{tab.name}</span>}
          key={tab.key}
          forceRender
        >
        </TabPane>)}
      </Tabs>
      <div className="main-page__content">
        <Routes>
          <Route path={"/finances"} element={<Operations />}></Route>
          <Route path={"/in-progress"} element={<AdCompaniesProgress businessId={businessId} />}></Route>
          <Route path={"/results"} element={<AdCompanies businessId={businessId} />}></Route>
          <Route path={"/influencers"} element={ <Influencers />}>
          </Route>
        </Routes>
      </div>
    </Content>
  );
}

export default MainPage;
