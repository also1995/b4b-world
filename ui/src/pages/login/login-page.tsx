import React from "react";
import {LoginForm} from "../../components/login-form/login-form";
import "./login-page.less";

export const LoginPage: React.FC = () => {
  return <div className="login-page__container">
    <img className="login-page__image" src="b4b_form.png"/>
    <LoginForm />
  </div>
}