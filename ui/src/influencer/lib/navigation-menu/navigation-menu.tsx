import './navigation-menu.less';
import React, {useContext} from 'react';
import { useCls } from '../../hooks';
import { Link } from 'react-router-dom';
import {AuthContext} from "../../../contexts/auth.provider";

interface NavigationItem {
  title: React.ReactNode;
  route: string;
}
export interface NavigationMenuProps {
  items: NavigationItem[];
}

export const NavigationMenu: React.FC<NavigationMenuProps> = ({ items }) => {
  const cls = useCls('navigation-menu');
  const {logout} = useContext(AuthContext);

  return (
    <div className={cls()}>
      {items.map(({ route, title }) => (
        <div key={route}>
          <Link to={route}>{title}</Link>
        </div>
      ))}
      <span onClick={() => logout()}>Log out</span>
    </div>
  );
};

export default NavigationMenu;
