import './ad-campaign-card.less';
import React, {useCallback, useContext, useState} from 'react';
import { useCls } from '../../hooks';
import { Button, Col, Form, Input, Row, Typography } from 'antd';
import { BriefLinkIcon } from '../icons';
import Icon from '../icon/icon';
import classnames from 'classnames';
import Countdown from '../countdown/countdown';
import {hashconnectService} from "../../hashconnect";
import moment from "moment";
import {BalanceContext} from "../../hooks/useBalance";

const { Text, Link, Title } = Typography;

export interface AdCampaignCardProps extends React.HTMLProps<HTMLDivElement> {
  brandName: string;
  href: string;
  linkText: string;
  briefHref: string;
  date: Date;
  reward: string;
  b4bReward: number;
  dueTo: Date;
  color?: string;
  isActionsVisible?: boolean;
  isLinkFormVisible?: boolean;
  isCountdownVisible?: boolean;
  isStartInNDaysVisible?: boolean;
}

export const AdCampaignCard: React.FC<AdCampaignCardProps> = ({
  dueTo,
  brandName,
  briefHref,
  href,
  date,
  linkText,
  reward,
  b4bReward,
  className,
  color = 'purple',
  isActionsVisible = true,
  isLinkFormVisible = false,
  isCountdownVisible = false,
  isStartInNDaysVisible = false,
  ...rest
}) => {
  const cls = useCls('ad-campaign-card');
  const [isLoading, setIsLoading] = useState(false);
  const {setBalance, balance, setUsdcBalance, usdcBalance} = useContext(BalanceContext);

  const onConfirm = useCallback(async() => {
    setIsLoading(true);
    try {
      await hashconnectService.approveBooking(moment(date).format('YYYY-MM-DD'));
    } catch (e) {
      console.error(e);
    }
    setBalance(balance + b4bReward);
  }, [date]);

  return (
    <div
      className={classnames(
        cls({
          color,
        }),
        className
      )}
      {...rest}
    >
      <div className={cls('brand-info')}>
        <Text>
          <b>{brandName}</b>
        </Text>
        <Link href={href}>{linkText}</Link>
      </div>
      <div className={cls('brief-info')}>
        <Link className={cls('brief-link')} href={briefHref}>
          Brief&nbsp;
          <Icon>
            <BriefLinkIcon />
          </Icon>
        </Link>
        <Text>{moment(dueTo).format('DD-MM-YYYY')}</Text>
        <Text>
          <b>{reward}</b>
        </Text>
      </div>
      {isCountdownVisible && <Countdown className={cls('timer')} dueTo={date}/>}
      <div className={cls('pending-footer')}>
        {isStartInNDaysVisible && <Title level={4}>
            START&nbsp;IN&nbsp;{moment(dueTo).diff(moment(), 'day')}&nbsp;DAYS
        </Title>}
      </div>
      {isLinkFormVisible && (
        <div className={cls('link-form')}>
          <Form>
            <Form.Item noStyle>
              <Input placeholder={'Link'} />
            </Form.Item>
          </Form>
          <Row justify={'center'}>
            <Col span={16}>
              <Button
                className={cls('confirm-button')}
                size={'large'}
                type={'primary'}
                block
                onClick={() => setUsdcBalance(usdcBalance + 250)}
              >
                <b>Send Link</b>
                <span className={cls('confirm-button-subtext')}>
                  +{b4bReward}&nbsp;B4B&nbsp;Points
                </span>
              </Button>
            </Col>
          </Row>
        </div>
      )}
      {isActionsVisible && (
        <div className={cls('actions')}>
          <Button className={cls('reject-button')} size={'large'}>
            Reject
          </Button>
          <Button
            className={cls('confirm-button')}
            size={'large'}
            type={'primary'}
            onClick={onConfirm}
            loading={isLoading}
          >
            <b>Confirm</b>
            <span className={cls('confirm-button-subtext')}>
              +{b4bReward}&nbsp;B4B&nbsp;Points
            </span>
          </Button>
        </div>
      )}
    </div>
  );
};

export default AdCampaignCard;
