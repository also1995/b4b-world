import './app-header.less';
import {B4BIcon, MenuIcon} from '../icons';
import {useCls} from '../../hooks';
import Balance from '../balance/balance';
import Icon from '../icon/icon';
import React, {useContext} from 'react';
import {Link} from "react-router-dom";
import {AppRoutes} from "../../app/routes";
import {BalanceContext} from "../../hooks/useBalance";
import CountUp from 'react-countup';

export interface AppHeaderProps {
  onRightIconClick: React.MouseEventHandler<HTMLSpanElement>;
  rightIcon?: React.ReactNode;
  middleContent?: React.ReactNode;
}

export const AppHeader: React.FC<AppHeaderProps> = ({
  onRightIconClick,
  rightIcon,
  middleContent,
}) => {
  const cls = useCls('app-header');
  const {balance} = useContext(BalanceContext);

  return (
    <div className={cls()}>
      <B4BIcon/>
      <Link to={AppRoutes.POPOVER_BALANCE_WALLET}>
        <span>{middleContent ?? <Balance>
            <CountUp end={balance} duration={1}/>
        </Balance>}</span>
      </Link>
      <Icon onClick={onRightIconClick}>
        {rightIcon ? rightIcon : <MenuIcon/>}
      </Icon>
    </div>
  );
};

export default AppHeader;
