import './countdown.less';
import { TimerIcon } from '../icons';
import React from 'react';
import classnames from 'classnames';
import { useCls } from '../../hooks/useCls';
import { Typography } from 'antd';
import { DateTime } from 'luxon';
import CountdownCore from '../countdown-core/countdown-core';

const { Title, Text } = Typography;

export interface CountdownProps extends React.HTMLProps<HTMLDivElement> {
  dueTo: Date;
}

export const Countdown: React.FC<CountdownProps> = ({ className, dueTo }) => {
  const cls = useCls('countdown');

  const dueToLuxon = DateTime.fromJSDate(dueTo);

  const daysLeft = dueToLuxon.diffNow('days').days;
  const isDaysCountdown = daysLeft >= 1;

  return (
    <div className={classnames(cls('timer'), className)}>
      <span>
        {isDaysCountdown && (
          <span className={cls('start-in')}>
            <TimerIcon />
            <Text className={cls('start-in-text')}>Start In</Text>
            <Text>{daysLeft.toFixed(0)}&nbsp;DAYS</Text>
          </span>
        )}

        <span style={{ display: isDaysCountdown ? 'none' : 'inline-block' }}>
          <CountdownCore dueTo={dueTo}>
            {(result) => <Title level={3}>{result}</Title>}
          </CountdownCore>
        </span>
      </span>
    </div>
  );
};

export default Countdown;
