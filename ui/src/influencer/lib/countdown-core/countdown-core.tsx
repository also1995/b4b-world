import './countdown-core.less';
import React, { useCallback, useEffect, useState } from 'react';
import { DateTime } from 'luxon';

export interface CountdownCoreProps {
  dueTo: Date;
  children: (result: string) => React.ReactNode;
}

export const CountdownCore: React.FC<CountdownCoreProps> = ({
  dueTo,
  children,
}) => {
  const [result, setResult] = useState('');
  const [intervalId, setIntervalId] = useState<number>(-1);

  const refresh = useCallback(() => {
    const dueToLuxon = DateTime.fromJSDate(dueTo);
    setResult(dueToLuxon.diffNow().toFormat('hh:mm:ss'));
  }, [dueTo]);

  useEffect(() => {
    const t = setInterval(() => {
      refresh();
    }, 1000);
    setIntervalId(+t);

    return () => {
      clearInterval(intervalId);
    };
  }, []);

  return <>{children(result)}</>;
};

export default CountdownCore;
