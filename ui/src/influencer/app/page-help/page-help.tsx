import './page-help.less';
import { Collapse } from 'antd';

const helpData = [
  {
    heading: 'What are B4B Points?',
    content:
      'B4B Points - individual rating indicator.\n' +
      'B4B Points credibly link their holders to the source of reputation on the platform. \n' +
      'The Rating support activity on the platform - speed of accepting requests, in-date campaign publishing.\n',
  },
  {
    heading: 'What is the NFT pass?',
    content:
      'Influencers who want additional Rewards based on B4B Points and Monthly Income should have NFTs with parameters suitable for their personality.',
  },
  {
    heading: 'What are B4B Coins?',
    content:
      'B4B Coins - growing, valuable and transferable tokens.\n' +
      'To create a form of liquid value attached to reputation, B4B Coins accrue to B4B Point and NFT holders through a series of dividends. Each B4B Point holder is awarded B4B Coins based on the number of points, NFT parameters, and Monthly Income.\n' +
      'Unlike B4B points, the total number of B4B coins is limited to give them value as a currency.\n' +
      'Our platform has a long-run limit on coin supply — there are only so many of them that can ever be minted.\n' +
      'The average total dividend will decrease yearly.\n' +
      '\n',
  },
  {
    heading: 'How to get more B4B Points?',
    content:
      'B4B Points earn based on:\n' +
      '-Activity on the platform - the speed of confirmation of an ad request\n' +
      '-Quality of ads - Execution of advertising on time and quality score\n' +
      '-Quantity of ads - Do more ads on the platform to get more rewards\n' +
      '-Repeat campaigns with the same brand\n',
  },
  {
    heading:
      'What amount of payment will I receive from the brand for advertising?',
    content:
      'On the B4B platform, influencers themselves set a price for their advertising',
  },
];

/* eslint-disable-next-line */
export interface PageHelpProps {}

export function PageHelp(props: PageHelpProps) {
  return (
    <Collapse>
      {helpData.map(({ heading, content }) => (
        <Collapse.Panel key={heading} header={heading}>
          {content}
        </Collapse.Panel>
      ))}
    </Collapse>
  );
}

export default PageHelp;
