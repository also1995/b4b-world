export enum AppRoutes {
  ROOT = '/',
  MY_SCHEDULE = '/my-schedule',
  ADS_CAMPAIGNS = '/ads-campaigns',
  MENU = '/menu',
  POPOVER_ROOT = '/popover',
  POPOVER_PROFILE = '/popover/profile',
  POPOVER_HISTORY = '/popover/history',
  POPOVER_HELP = '/popover/help',
  POPOVER_BALANCE_ROOT = '/popover/balance',
  POPOVER_BALANCE_PLATFORM = '/popover/balance/platform',
  POPOVER_BALANCE_WALLET = '/popover/balance/wallet',
  POPOVER_BALANCE_WALLET_CONNECT = '/popover/balance/wallet/connect',
}
