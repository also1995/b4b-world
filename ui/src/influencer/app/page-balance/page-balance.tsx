import './page-balance.less';
import { Button, Typography } from 'antd';
import {useContext, useState} from 'react';
import QRCode from 'react-qr-code';
import {useCls} from "../../hooks";
import {useHashConnect} from "../../../hooks/useHashConnect";
import {useHashPackBalance} from "../../../hooks/useHashPackBalance";
import ContainerBasePage from "../../lib/container-base-page/container-base-page";
import CoinsBalance from "../../lib/coins-balance/coins-balance";
import {shorten} from "../../../utils/shorten";
import ContainerBaseLayer from "../../lib/container-base-layer/container-base-layer";
import {hashconnectService} from "../../hashconnect";
import {BalanceContext} from "../../hooks/useBalance";

const { Text } = Typography;

/* eslint-disable-next-line */
export interface PageBalanceProps {}

export const PageBalance = (props: PageBalanceProps) => {
  const cls = useCls('page-balance');
  const [isConnectingHashConnect, setIsConnectingHashConnect] = useState(false);

  const { pairingString, isPaired } = useHashConnect();
  const hbarBalance = useHashPackBalance();
  const {balance, usdcBalance} = useContext(BalanceContext);

  return (
    <ContainerBasePage
      className={cls()}
      footer={
        <div className={cls('footer')}>
          <Button block type={'primary'} size={'large'}>
            Transfer
          </Button>
        </div>
      }
    >
      <Text className={cls('gray-text')}>Address</Text>
      <ContainerBaseLayer>
        {!hashconnectService.saveData.pairedAccounts[0] && (
          <Text type={'secondary'}>HashPack account not connected</Text>
        )}
        <b>{hashconnectService.saveData.pairedAccounts[0]}</b>
      </ContainerBaseLayer>
      <CoinsBalance
        coins={[
          { balance: `${usdcBalance}`, ticker: 'USDC' },
          { balance: `${balance}`, ticker: 'B4B Points' },
          { balance: '0', ticker: 'B4B Coins' },
        ]}
      />
      {hbarBalance && (
        <CoinsBalance
          coins={[
            {
              balance: hbarBalance,
              ticker: 'HBAR',
            },
          ]}
        />
      )}
      {!isPaired && !isConnectingHashConnect && (
        <Button
          type={'primary'}
          size={'large'}
          onClick={() => setIsConnectingHashConnect(true)}
        >
          Connect HashPack Account
        </Button>
      )}
      {!isPaired && isConnectingHashConnect && pairingString && (
        <div className={cls('qr-code-container')}>
          <Text>Please scan the following QR-code to connect wallet:</Text>
          <QRCode value={pairingString} />
          <Text>Or paste the pairstring:</Text>
          <Text
            copyable={{
              text: pairingString,
            }}
          >
            <b>{shorten(pairingString)}</b>
          </Text>
        </div>
      )}
    </ContainerBasePage>
  );
};

export default PageBalance;
