import './page-history.less';
import { Typography } from 'antd';
import {useCls} from "../../hooks";

const { Title } = Typography;

/* eslint-disable-next-line */
export interface PageHistoryProps {}

export function PageHistory(props: PageHistoryProps) {
  const cls = useCls('page-history');

  return (
    <div className={cls()}>
      <Title level={4} className={cls('title')}>
        History
      </Title>
    </div>
  );
}

export default PageHistory;
