import './page-ads-campaigns.less';
import { useState } from 'react';
import {useCls} from "../../hooks";
import ContainerBasePage from "../../lib/container-base-page/container-base-page";
import ButtonGroupToggler from "../../lib/button-group-toggler/button-group-toggler";
import AdCampaignCard from "../../lib/ad-campaign-card/ad-campaign-card";
import {useAdsCampaigns} from "../../hooks/useAdsCampaigns";

const colors: Record<number, string> = {
  0: 'purple',
  1: 'blue',
  2: 'cyan',
};

/* eslint-disable-next-line */
export interface PageAdsCampaignsProps {}

export function PageAdsCampaigns(props: PageAdsCampaignsProps) {
  const cls = useCls('page-ads-campaigns');
  const [currentTogglerIndex, setCurrentTogglerIndex] = useState(0);
  const {newCards, pending, inProgress, approveCard, sendLink} = useAdsCampaigns();

  return (
    <ContainerBasePage className={cls()}>
      <div
        className={cls('background', {
          color: colors[currentTogglerIndex],
        })}
      />
      <ButtonGroupToggler
        currentTogglerIndex={currentTogglerIndex}
        togglers={[
          { name: 'New' },
          { name: 'Pending' },
          { name: 'In Progress' },
        ]}
        setCurrentTogglerIndex={setCurrentTogglerIndex}
      />
      <div className={cls('ads-container')}>
        {
          (currentTogglerIndex === 0 ? newCards : (currentTogglerIndex === 1 ? pending : inProgress)).map(({dueTo, brandName, briefHref, href, date, linkText, reward, b4bReward, sentLink, confirmed, id}) => <AdCampaignCard
            key={id}
            brandName={brandName}
            dueTo={dueTo}
            date={date}
            href={href}
            linkText={linkText}
            reward={reward}
            briefHref={briefHref}
            b4bReward={b4bReward}
            color={colors[currentTogglerIndex]}
            isActionsVisible={currentTogglerIndex === 0}
            isLinkFormVisible={currentTogglerIndex === 2}
            onClick={() => {
              if (currentTogglerIndex === 0) {
                approveCard(id);
              }
              if (currentTogglerIndex === 2) {
                sendLink(id, 'link');
              }
            }}
            isCountdownVisible={currentTogglerIndex === 0}
            isStartInNDaysVisible={currentTogglerIndex === 1}
          />)
        }
      </div>
    </ContainerBasePage>
  );
}

export default PageAdsCampaigns;
