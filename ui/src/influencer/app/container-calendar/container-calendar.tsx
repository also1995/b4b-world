import './container-calendar.less';
import React from 'react';
import classnames from 'classnames';
import {useCls} from "../../hooks";

export interface CalendarContainerProps
  extends React.HTMLProps<HTMLDivElement> {
  children: React.ReactNode;
}

export const ContainerCalendar: React.FC<CalendarContainerProps> = ({
  children,
  className,
}) => {
  const cls = useCls('calendar-container');

  return <div className={classnames(cls(), className)}>{children}</div>;
};

export default ContainerCalendar;
