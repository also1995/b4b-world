import PageMySchedule from './page-my-schedule/page-my-schedule';
import PageAdsCampaigns from './page-ads-campaigns/page-ads-campaigns';
import {useNavigate, useLocation, Routes, Route} from 'react-router-dom';
import {useCallback, useRef} from 'react';
import {AppRoutes} from './routes';
import './app.less';
import PageProfile from './page-profile/page-profile';
import PageHistory from './page-history/page-history';
import PageHelp from './page-help/page-help';
import PageBalance from './page-balance/page-balance';
import {useCls} from "../hooks";
import AppHeader from "../lib/app-header/app-header";
import {CrossIcon, MenuIcon} from "../lib/icons";
import ContainerSwipeContent from "../lib/container-swipe-content/container-swipe-content";
import ContainerPopover from "../lib/container-popover/container-popover";
import NavigationMenu from "../lib/navigation-menu/navigation-menu";
import {BalanceContext, useBalance} from "../hooks/useBalance";

export function App() {
  const navigate = useNavigate();
  const location = useLocation();
  const cls = useCls('app');
  const appContentRef = useRef(null);
  const {balance, setBalance, usdcBalance, setUsdcBalance} = useBalance();

  const onMenuClick = useCallback(() => {
    navigate(AppRoutes.MENU);
  }, [navigate]);
  const onBack = useCallback(() => {
    navigate(AppRoutes.ROOT);
  }, [navigate]);

  const isMenuOpened = location.pathname.startsWith(AppRoutes.MENU);
  const isPopoverOpened = location.pathname.startsWith(AppRoutes.POPOVER_ROOT);

  const isAppHeaderAltState = isMenuOpened || isPopoverOpened;

  return (
    <BalanceContext.Provider value={{balance, setBalance, usdcBalance, setUsdcBalance}}>
      <div className={cls()}>
        <AppHeader
          onRightIconClick={isAppHeaderAltState ? onBack : onMenuClick}
          rightIcon={isAppHeaderAltState ? <CrossIcon/> : <MenuIcon/>}
          middleContent={isAppHeaderAltState ? <span/> : undefined}
        />
        <div className={cls('content')} ref={appContentRef}>
          <ContainerSwipeContent
            pages={[
              {
                content: <PageMySchedule/>,
                key: '1',
                title: 'My Schedule',
                route: AppRoutes.MY_SCHEDULE,
              },
              {
                content: <PageAdsCampaigns/>,
                key: '2',
                title: 'Ads Campaigns',
                route: AppRoutes.ADS_CAMPAIGNS,
              },
            ]}
          />
          <ContainerPopover
            direction={isMenuOpened ? 'rl' : 'lr'}
            isOpened={isMenuOpened}
          >
            <NavigationMenu
              items={[
                {route: AppRoutes.MY_SCHEDULE, title: 'My Schedule'},
                {route: AppRoutes.ADS_CAMPAIGNS, title: 'Ads Campaigns'},
                {
                  route: AppRoutes.POPOVER_BALANCE_WALLET,
                  title: 'Wallet Balance',
                },
                {
                  route: AppRoutes.POPOVER_BALANCE_PLATFORM,
                  title: 'Platform Balance',
                },
                {route: AppRoutes.POPOVER_PROFILE, title: 'Profile'},
                // {route: AppRoutes.POPOVER_HISTORY, title: 'History'},
                {route: AppRoutes.POPOVER_HELP, title: 'Help'},
              ]}
            />
          </ContainerPopover>
          <ContainerPopover
            direction={isPopoverOpened ? 'bt' : 'tb'}
            isOpened={isPopoverOpened}
          >
            <Routes>
              <Route path={AppRoutes.POPOVER_ROOT}>
                <Route
                  path={AppRoutes.POPOVER_PROFILE}
                  element={<PageProfile/>}
                />
                <Route
                  path={AppRoutes.POPOVER_HISTORY}
                  element={<PageHistory/>}
                />
                <Route path={AppRoutes.POPOVER_HELP} element={<PageHelp/>}/>
              </Route>
              <Route
                path={`${AppRoutes.POPOVER_BALANCE_ROOT}/*`}
                element={
                  <ContainerSwipeContent
                    pages={[
                      {
                        content: <PageBalance/>,
                        key: '1',
                        title: 'Platform Account',
                        route: AppRoutes.POPOVER_BALANCE_PLATFORM,
                      },
                      {
                        content: <PageBalance/>,
                        key: '2',
                        title: 'Wallet Account',
                        route: AppRoutes.POPOVER_BALANCE_WALLET,
                      },
                    ]}
                  />
                }
              />
            </Routes>
          </ContainerPopover>
        </div>
      </div>
    </BalanceContext.Provider>
  );
}

export default App;
