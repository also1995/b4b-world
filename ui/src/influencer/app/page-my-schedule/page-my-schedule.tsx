import './page-my-schedule.less';
import { Button, Calendar, Typography } from 'antd';
import ContainerCalendar from '../container-calendar/container-calendar';
import { useCallback, useEffect, useState } from 'react';
import type { Moment } from 'moment';
import moment from 'moment';
import {useCls} from "../../hooks";
import ContainerBasePage from "../../lib/container-base-page/container-base-page";
import ContainerBlock from "../../lib/container-block/container-block";
import {hashconnectService} from "../../hashconnect";

const { Text } = Typography;

/* eslint-disable-next-line */
export interface PageMyScheduleProps {}

export function PageMySchedule() {
  const cls = useCls('page-my-schedule');
  const [isEditing, setIsEditing] = useState(false);
  const [selectedCells, setSelectedCells] = useState<Element[]>([]);
  const [bookedDates, setBookedDates] = useState<Moment[]>(
    [7, 9, 10].map((day) => moment().add({ day }))
  );

  const onSelect = useCallback(
    (date: Moment) => {
      if (!isEditing) {
        return;
      }

      const dateString = date.format('YYYY-MM-DD');

      const el = document.querySelector(`td[title="${dateString}"]`);
      if (!el) {
        throw new Error('Element not found');
      }

      let newSelectedCells = selectedCells.filter(
        (currentEl) => currentEl.getAttribute('title') !== dateString
      );
      if (newSelectedCells.length === selectedCells.length) {
        newSelectedCells = [...selectedCells];
        newSelectedCells.push(el);
        hashconnectService.createTimeSlot(dateString);
      }
      setSelectedCells(newSelectedCells);
    },
    [isEditing, selectedCells]
  );

  useEffect(() => {
    selectedCells.forEach((el) => {
      el.classList.add('ant-picker-cell-selected');
    });

    return () => {
      selectedCells.forEach((el) => {
        el.classList.remove('ant-picker-cell-selected');
      });
    };
  }, [selectedCells]);

  useEffect(() => {
    bookedDates.forEach((dateString) => {
      document
        .querySelector(`td[title="${dateString.format('YYYY-MM-DD')}"]`)
        ?.classList.add('ant-picker-cell-selected-2');
    });
  }, [bookedDates]);

  return (
    <ContainerBasePage
      footer={
        <ContainerBlock centered className={cls('footer')}>
          {!isEditing && (
            <Button
              type={'primary'}
              size={'large'}
              block
              onClick={() => setIsEditing(true)}
            >
              Edit
            </Button>
          )}
          {isEditing && (
            <Button
              type={'primary'}
              size={'large'}
              block
              onClick={() => {
                setIsEditing(false);
              }}
            >
              Save
            </Button>
          )}
        </ContainerBlock>
      }
    >
      <ContainerCalendar className={cls('calendar')}>
        {isEditing && (
          <Text className={cls('calendar-subtext')}>
            Choose the empty dates for ads
          </Text>
        )}
        <div
          onClick={(event) => {
            if (!isEditing) {
              event.stopPropagation();
            }
          }}
        >
          <Calendar fullscreen={false} onSelect={onSelect} value={moment()} />
        </div>
      </ContainerCalendar>
    </ContainerBasePage>
  );
}

export default PageMySchedule;
