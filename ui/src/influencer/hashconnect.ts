import {HashConnect, HashConnectTypes, MessageTypes} from 'hashconnect';
import {EventEmitter} from 'events';
import {
  ContractExecuteTransaction,
  ContractCallQuery,
  Hbar,
  Client,
  PrivateKey,
  AccountId,
  ContractFunctionParameters,
} from '@hashgraph/sdk';
import {Interface} from '@ethersproject/abi';
import lodash from 'lodash';
import {HashConnectSigner} from "hashconnect/dist/provider/signer";

const GAS_CONTRACT = 3000000;

const QueryContract = async (
  signer: any,
  {contractId, method, params}: any
) => {
  const contractCallResult = await new ContractCallQuery()
    .setContractId(contractId)
    .setGas(GAS_CONTRACT)
    .setQueryPayment(new Hbar(10))
    .setFunction(method, params)
    .executeWithSigner(signer);


  if (contractCallResult?.errorMessage) {
    throw `error calling contract: ${contractCallResult.errorMessage}`;
  }

  return contractCallResult;
};

const CallContract = async (
  signer: any,
  {contractId, method, params = null}: any
) => {
  const contractTransaction = await new ContractExecuteTransaction()
    .setContractId(contractId)
    .setGas(GAS_CONTRACT)
    .setFunction(method, params)
    .freezeWithSigner(signer);

  const contractTransactionResponse =
    await contractTransaction.executeWithSigner(signer);

  const contractReceipt = await signer.provider.waitForReceipt(contractTransactionResponse);

  return contractReceipt.status.toString() === 'SUCCESS';
};

const getCompiledContractJson = (contract: string) => {
  try {
    return require(`${process.cwd()}/artifacts/contracts/${contract}.sol/${contract}.json`);
  } catch (e) {
    throw (
      "Unable to find compiled contract for '" +
      contract +
      "', has it been compiled with 'hardhat compile' or is the name correct?"
    );
  }
};

const SubscribeToEmittedEvents = async (
  client: Client,
  {contractId, method, contract, params = null}: any
) => {
  // Will look into refactoring this func. to work with call ~
  // Can check against abi to see if there is event associated with func.

  const parsedJson = getCompiledContractJson(contract);
  const abiInterface = new Interface(parsedJson.abi);

  const contractTransaction = await new ContractExecuteTransaction()
    .setContractId(contractId)
    .setGas(GAS_CONTRACT)
    .setFunction(method, params)
    .freezeWith(client);

  const contractTransactionResponse = await contractTransaction.execute(client);
  const record = await contractTransactionResponse.getRecord(client);

  // look through logs returned with getRecord.
  const events = record.contractFunctionResult?.logs.map((log) => {
    // lets make it a little more readable.
    const logStrHex = '0x'.concat(Buffer.from(log.data).toString('hex'));
    const logTopics = log.topics.map((topic) => {
      return '0x'.concat(Buffer.from(topic).toString('hex'));
    });
    return abiInterface.parseLog({data: logStrHex, topics: logTopics});
  });
  // to read this: events[i].args.$param
  return events;
};

const Hashgraph = (signer: HashConnectSigner) => ({
  contract: {
    call: (params: any) => CallContract(signer, params),
    query: (params: any) => QueryContract(signer, params),
    // sub: (params: any) => SubscribeToEmittedEvents(client, params),
  },
});

const contractId = '0.0.34826146';
const pointsContractId = '0.0.34826141';

export class HashconnectService extends EventEmitter {
  hashconnect!: HashConnect;
  client?: Client;
  status = 'Initializing';

  availableExtensions: HashConnectTypes.WalletMetadata[] = [];

  saveData: {
    topic: string;
    pairingString: string;
    privateKey?: string;
    pairedWalletData?: HashConnectTypes.WalletMetadata;
    pairedAccounts: string[];
  } = {
    topic: '',
    pairingString: '',
    privateKey: undefined,
    pairedWalletData: undefined,
    pairedAccounts: [],
  };

  appMetadata: HashConnectTypes.AppMetadata = {
    name: 'B4B',
    description: 'B4B Influencer App',
    icon: 'https://thumb.tildacdn.com/tild3037-3364-4462-b833-323863376532/-/resize/580x/-/format/webp/___2.png',
  };

  constructor() {
    super();
    this.initHashconnect();
  }

  async initHashconnect() {
    //create the hashconnect instance
    this.hashconnect = new HashConnect(true);
    if (!this.loadLocalData()) {
      //first init, store the private key in localstorage
      const initData = await this.hashconnect.init(this.appMetadata);
      this.saveData.privateKey = initData.privKey;

      //then connect, storing the new topic in localstorage
      const state = await this.hashconnect.connect();
      console.log('Received state', state);
      this.saveData.topic = state.topic;

      //generate a pairing string, which you can display and generate a QR code from
      this.saveData.pairingString = this.hashconnect.generatePairingString(
        state,
        'testnet',
        true
      );

      //find any supported local wallets
      this.hashconnect.findLocalWallets();

      this.status = 'Connected';
      this.emit('status');
    } else {
      await this.hashconnect.init(this.appMetadata, this.saveData.privateKey);
      await this.hashconnect.connect(
        this.saveData.topic,
        this.saveData.pairedWalletData!
      );

      this.status = 'Paired';
      this.emit('status');
    }

    this.setUpEvents();
  }

  setUpEvents() {
    this.hashconnect.foundExtensionEvent.on((data) => {
      this.availableExtensions.push(data);
      console.log('Found extension', data);
    });

    // this.hashconnect.additionalAccountResponseEvent.on((data) => {
    //     console.log("Received account info", data);

    //     data.accountIds.forEach(id => {
    //         if(this.saveData.pairedAccounts.indexOf(id) == -1)
    //             this.saveData.pairedAccounts.push(id);
    //     })
    // })

    this.hashconnect.pairingEvent.on((data) => {
      console.log('Paired with wallet', data);
      this.status = 'Paired';
      this.emit('status');

      this.saveData.pairedWalletData = data.metadata;

      data.accountIds.forEach((id) => {
        if (this.saveData.pairedAccounts.indexOf(id) == -1)
          this.saveData.pairedAccounts.push(id);
      });

      this.saveDataInLocalstorage();
    });

    this.hashconnect.transactionEvent.on((data) => {
      //this will not be common to be used in a dapp
      console.log('transaction event callback');
    });
  }

  async connectToExtension() {
    this.hashconnect.connectToLocalWallet(this.saveData.pairingString);
  }

  async sendTransaction(
    trans: Uint8Array,
    acctToSign: string,
    return_trans = false
  ) {
    const transaction: MessageTypes.Transaction = {
      topic: this.saveData.topic,
      byteArray: trans,

      metadata: {
        accountToSign: acctToSign,
        returnTransaction: return_trans,
      },
    };

    return await this.hashconnect.sendTransaction(
      this.saveData.topic,
      transaction
    );
  }

  async requestAccountInfo() {
    const request: MessageTypes.AdditionalAccountRequest = {
      topic: this.saveData.topic,
      network: 'mainnet',
      multiAccount: true,
    };

    await this.hashconnect.requestAdditionalAccounts(
      this.saveData.topic,
      request
    );
  }

  saveDataInLocalstorage() {
    const data = JSON.stringify(this.saveData);

    localStorage.setItem('hashconnectData', data);
  }

  loadLocalData(): boolean {
    const foundData = localStorage.getItem('hashconnectData');

    if (foundData) {
      this.saveData = JSON.parse(foundData);
      console.log('Found local data', this.saveData);
      return true;
    } else return false;
  }

  clearPairings() {
    this.saveData.pairedAccounts = [];
    this.saveData.pairedWalletData = undefined;
    this.status = 'Connected';
    this.emit('status');
    localStorage.removeItem('hashconnectData');
  }

  async getCurrentAccountBalance() {
    const accountId = this.saveData.pairedAccounts[0];
    const provider = this.hashconnect.getProvider(
      'testnet',
      this.saveData.topic,
      accountId
    );

    return (await provider.getAccountBalance(accountId)).toString();
  }

  async createTimeSlot(dateString: string) {
    if (!this.saveData.pairedAccounts[0] || !this.saveData.privateKey) {
      throw new Error('Account ID or private key is falsy');
    }
    const accountId = this.saveData.pairedAccounts[0];

    const provider = this.hashconnect.getProvider(
      'testnet',
      this.saveData.topic,
      accountId
    );

    const signer = this.hashconnect.getSigner(provider);

    const hashgraph = Hashgraph(signer);

    const time = Math.floor(new Date(dateString).getTime() / 1000);

    // creatTimeSlot
    const response1 = await hashgraph.contract.call({
      contractId: contractId,
      method: 'createTimeSlot',
      params: new ContractFunctionParameters().addUint256(time),
    });

    console.log(response1);
  }

  async approveBooking(dateString: string) {
    if (!this.saveData.pairedAccounts[0] || !this.saveData.privateKey) {
      throw new Error('Account ID or private key is falsy');
    }
    const accountId = this.saveData.pairedAccounts[0];

    const provider = this.hashconnect.getProvider(
      'testnet',
      this.saveData.topic,
      accountId
    );

    const signer = this.hashconnect.getSigner(provider);

    const hashgraph = Hashgraph(signer);
    const time = Math.floor(new Date(dateString).getTime() / 1000);

    const response3 = await hashgraph.contract.call({
      contractId,
      method: "approveBooking",
      params: new ContractFunctionParameters()
        .addUint256(time)
        .addUint256(0)
    });

    console.log(response3);
  }

  async requestBooking(dateTime: string, amount: number) {
    if (!this.saveData.pairedAccounts[0] || !this.saveData.privateKey) {
      throw new Error('Account ID or private key is falsy');
    }
    const accountId = this.saveData.pairedAccounts[0];

    const provider = this.hashconnect.getProvider(
      'testnet',
      this.saveData.topic,
      accountId
    );

    const signer = this.hashconnect.getSigner(provider);

    const hashgraph = Hashgraph(signer);
    const time = Math.floor(new Date(dateTime).getTime() / 1000);

    const response2 = await hashgraph.contract.call({
      contractId,
      method: "requestBooking",
      params: new ContractFunctionParameters()
        .addAddress(AccountId.fromString(accountId).toSolidityAddress())
        .addUint256(time)
        // @ts-ignore
        .addInt64( amount * 10000000)
    })
  }

  getBalanceFromSmartContract = lodash.once(async () => {
    if (!this.saveData.pairedAccounts[0] || !this.saveData.privateKey) {
      throw new Error('Account ID or private key is falsy');
    }
    const accountId = this.saveData.pairedAccounts[0];

    const provider = this.hashconnect.getProvider(
      'testnet',
      this.saveData.topic,
      accountId
    );

    const signer = this.hashconnect.getSigner(provider);

    const hashgraph = Hashgraph(signer);
    const address = AccountId.fromString(accountId).toSolidityAddress();

    const result = await hashgraph.contract.query({
      contractId: pointsContractId,
      method: "balanceOf",
      params: new ContractFunctionParameters()
        .addAddress(address)
    });

    // const balance = response.getAddress(0);
    const balance = result.getUint256(0).toString();
    console.log(balance);
    return balance;
  });
}

export const hashconnectService = new HashconnectService();
export const businessHashconnectService = new HashconnectService();
