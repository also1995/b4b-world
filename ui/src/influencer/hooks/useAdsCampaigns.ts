import moment from "moment";
import {useCallback, useState} from "react";

const defaultBrandName = 'Super Brand';
const defaultHref = 'https://superbrand.com';

interface Card {
  brandName: string;
  href: string;
  linkText: string;
  briefHref: string;
  date: Date;
  reward: string;
  b4bReward: number;
  dueTo: Date;
  confirmed: boolean;
  sentLink: string;
}

class RandomCard implements Card {
  brandName = defaultBrandName;
  href = defaultHref;
  linkText = defaultHref;
  briefHref = defaultHref;
  date: Date = moment().add({hour: Math.random() * 2 + 1}).toDate();
  b4bReward: number = 100;
  reward: string;
  dueTo: Date;
  confirmed = false;
  sentLink = '';


  constructor(dueTo: Date, confirmed = false, otherFields: Partial<Record<keyof Card, any>> = {}) {
    this.dueTo = dueTo;
    this.confirmed = confirmed;
    this.reward = `$${(250).toFixed(0)}`;
    for (const key in otherFields) {
      // @ts-ignore
      this[key] = otherFields[key];
    }
  }
}

const defaultCards: Record<string, Card> = {
  '0': new RandomCard(moment().add({hour: 4}).toDate(), false, {
    brandName: 'LetMeSpeak',
    linkText: 'letmespeak.org',
    href: 'https://www.letmespeak.org',
    briefHref: 'https://docs.google.com/document/d/1KkzEMgSWCNJ5p3HJHNdijZEkADOELybaxHSwlQQ1CKA/edit?usp=sharing',
  }),
  '1': new RandomCard(moment().add({day: 2}).toDate(), false, {
    brandName: 'Faceout',
    href: 'http://www.faceout.me/',
    linkText: 'faceout.me',
    briefHref: 'http://www.faceout.me/',
  }),
  '2': new RandomCard(moment().add({hour: 12}).toDate(), false, {
    brandName: 'AllRight',
    href: 'https://allright.com/',
    briefHref: 'https://allright.com',
    linkText: 'allright.com',
  }),
  '3': new RandomCard(moment().add({day: 4}).toDate(), true, {
    briefHref: 'https://speakpal.club/',
    href: 'https://speakpal.club/',
    linkText: 'speakpal.club',
    brandName: 'Speakpal'
  }),
}

export const useAdsCampaigns = () => {
  const [cards, setCards] = useState(defaultCards);

  const approveCard = useCallback((cardId: string) => {
    setCards({
      ...cards,
      [cardId]: {
        ...cards[cardId],
        confirmed: true,
      }
    })
  }, []);

  const sendLink = useCallback((cardId: string, link: string) => {
    setCards({
      ...cards,
      [cardId]: {
        ...cards[cardId],
        sentLink: link
      }
    })
  }, []);

  const cardsArray = Object.entries(cards).map(([key, value]) => ({
      ...value,
      id: key
    }));
  return {
    newCards: cardsArray.filter(({confirmed}) => !confirmed).sort((a, b) => +a.dueTo - +b.dueTo),
    pending: cardsArray.filter(({confirmed, dueTo}) => confirmed && moment(dueTo).diff(moment(), 'day') >= 1).sort((a, b) => +a.dueTo - +b.dueTo),
    inProgress: cardsArray.filter(({confirmed, dueTo}) => confirmed && moment(dueTo).diff(moment(), 'day') < 1).sort((a, b) => +a.dueTo - +b.dueTo),
    approveCard,
    sendLink
  }
}