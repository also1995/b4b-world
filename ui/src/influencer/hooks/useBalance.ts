import React, {useState} from "react";
import {noop} from "lodash";

interface IBalanceContext {
  balance: number;
  setBalance: ((newBalance: number) => void) | ((oldBalance: number) => number);
  usdcBalance: number;
  setUsdcBalance: (newBalance: number) => void;
}

export const BalanceContext = React.createContext<IBalanceContext>({
  balance: 20,
  setBalance: noop,
  usdcBalance: 0,
  setUsdcBalance: noop,
});

export const useBalance = () => {
  const [balance, setBalance] = useState(20);
  const [usdcBalance, setUsdcBalance] = useState(0);

  return {
    balance,
    setBalance,
    usdcBalance,
    setUsdcBalance
  }
}