import { ConfigProvider } from 'antd';
import { environment } from './environments/environment';

ConfigProvider.config({
  ...environment.antdConfig,
});
