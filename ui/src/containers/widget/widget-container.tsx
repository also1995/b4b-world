import React from "react";

import "./widget-container.less";
import {Divider} from "antd";

export interface WidgetContainerProps {
    title: string;
    children: React.ReactNode;
}

export const WidgetContainer: React.FC<WidgetContainerProps> = props => {
  return <div className="widget-container__container">
    <Divider orientation="left">{props.title}</Divider>
    <div className="widget-container__content">
      {props.children}
    </div>
  </div>
}
