import axios from 'axios'
import {useAuth} from "../hooks/use-auth";

const BASE_URL = `${process.env["REACT_APP_BACKEND_URL"]}/api/`;

export class ApiProvider {
  static async getRequest<T>(url: string, params?: any): Promise<T> {
    const {getBearer} = useAuth();
    const headers = {
      Authorization: `Bearer ${getBearer()}`
    };
    const result = await axios.get<T>(`${BASE_URL + url}`, {params, headers});
    return result.data;
  }

  static async postRequest<T>(url: string, body: any, headers?: any): Promise<T> {
    const {getBearer} = useAuth();
    headers = {
      ...headers,
      Authorization: `Bearer ${getBearer()}`
    };
    const result = await axios.post<T>(`${BASE_URL + url}`, body, {headers});
    return result.data;
  }

  static async deleteRequest<T>(url: string, headers?: any): Promise<T> {
    const {getBearer} = useAuth();
    headers = {
      ...headers,
      Authorization: `Bearer ${getBearer()}`
    };
    const result = await axios.delete<T>(`${BASE_URL + url}`, {headers});
    return result.data;
  }

  static async postFormData<T>(url: string, body: FormData): Promise<T> {
    return ApiProvider.postRequest<T>(url, body, {
      "Content-Type": 'multipart/form-data'
    });
  }
}
