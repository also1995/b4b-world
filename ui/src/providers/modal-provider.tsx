import React from "react";
import {Modal} from "antd";
import {ExclamationCircleOutlined} from "@ant-design/icons";

export function confirmation(content: string,
  onOk: () => void,
  onCancel?: () => void): void {
  Modal.confirm({
    title: 'Confirm',
    icon: <ExclamationCircleOutlined/>,
    content,
    okText: 'OK',
    cancelText: 'Cancel',
    onOk,
    onCancel
  });
}