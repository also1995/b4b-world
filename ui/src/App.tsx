import React  from 'react';
import {
  Route, BrowserRouter, Navigate, Routes
} from 'react-router-dom';
import AppHeader from './components/header/header';
import MainPage from './pages/main/main-page';
import {ConfigProvider, Layout} from 'antd';
import {LoginPage} from "./pages/login/login-page";
import {RegistrationPage} from "./pages/registration/registration-page";
import {AuthContext, AuthProvider} from "./contexts/auth.provider";
import EntryMain from "./pages/entry-main/EntryMain";
import {AdCompany} from "./pages/ad-company/ad-company";
import './App.less';
import {environment} from "./influencer/environments/environment";
import App from "./influencer/app/app";
import './influencer/swiper';

const AllApp: React.FC = () =>  {
  return (
    <Layout className="ui-base-layout">
        <BrowserRouter>
          <AuthProvider>
          {/*<Route path="/form/:id" exact>*/}
          {/*  <Certificate/>*/}
          {/*</Route>*/}
          <AuthContext.Consumer>
            {({isAuthorized, currentUser}) => (
              isAuthorized ? <>
                {
                      currentUser?.role !== "BLOGGER" ?
                        <>
                          <AppHeader/>
                          <Routes>
                            <Route path={"/business"} element={<EntryMain/>}></Route>
                            <Route
                              path={"/business/:businessId/*"} element={<MainPage/>}></Route>
                            <Route path={"/company/:companyId"} element={<AdCompany/>}></Route>
                            <Route path="/" element={currentUser ? currentUser.role === 'ADMIN' ? <Navigate to="/business"/> : <Navigate to={`/business/${currentUser.entity.id}/results`}/> : null} ></Route>
                          </Routes>
                        </> :
                        <>
                          <ConfigProvider prefixCls={environment.antdConfig.prefixCls}>
                              <App />
                          </ConfigProvider>
                        </>
                }
              </>
                :
                <>
                  <AppHeader/>
                  <Routes>
                    <Route path="/login" element={ <LoginPage/>}>

                    </Route>
                    <Route path="/registration" element={<RegistrationPage/>}>

                    </Route>
                    <Route path="/*" element={<Navigate to="/login" />} />
                  </Routes>
                </>
            )}
          </AuthContext.Consumer>
          </AuthProvider>
        </BrowserRouter>
    </Layout>
  );
}

export default AllApp;
