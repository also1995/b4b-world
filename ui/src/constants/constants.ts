import {LeadStatus} from "../models/lead";

export const EMPTY_SIGN = "-";

export const STATUS_COLOR_MAP = {
  [LeadStatus.ERROR]: "red",
  [LeadStatus.ACCEPTED]: "#b7eb8f",
  [LeadStatus.NEW]: "#824c97",
  [LeadStatus.IN_PROGRESS]: "blue"
}

export const RADIAN = Math.PI / 180;
