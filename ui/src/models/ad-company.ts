import {LeadStatisticItem} from "./lead";

export interface AdCompanyData {
    data: AdCompany[];
    coverage: number;
    clicks: number;
    leads: number;
}

export interface Video {
    promoCode: string
    viewCount: number
    clicks: number
    contacts: number
    payment: number;
    content: string;
    link: string;
}

export interface AdCompany {
	id: string;
	name: string;
	blogger: { // yes
		id: string;
		nick: string;
		link: string;
	},
	/**
	 * ISO формат
	 */
	publicationDate: string; // yes
	comment?: string;
	result: {
		coverage: number;
		clicks: number;
		leads: number;
	},
	promoCode: string;
	resultLink: string;
	link: string;
	utmLink: string;
	cost: number; // yes
	leadsStatistic?: LeadStatisticItem[];
	status: AdCompanyStatus;
	adFormat: AdFormat;
}

export enum AdCompanyStatus {
	POSTED = "POSTED",
	WAITING = 'WAITING',
	CONFIRMED = 'CONFIRMED',
	APPROVED = 'APPROVED',
}

export enum AdFormat {
	POST = "POST",
	POST_PLUS_PIN = "POST_PLUS_PIN"
}
