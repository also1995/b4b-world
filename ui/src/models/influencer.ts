export interface IInfluencer {
  id: string;
  name: string;
  raiting: number;
  socialMedia: string
  subscribers: number;
  er: number;
  price: number;
  pricePin: number;
}
