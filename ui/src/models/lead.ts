export enum LeadStatus {
    IN_PROGRESS = "IN_PROGRESS",
    ERROR = "ERROR",
    ACCEPTED = "ACCEPTED",
    NEW = "NEW",
}

export interface Lead {
    id: string;
    name: string;
    phone: string;
    status: LeadStatus;
}

export interface LeadStatisticItem {
    status: LeadStatus;
    value: number;
}