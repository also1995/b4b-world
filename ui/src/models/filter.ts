import {ReactNode} from "react";

export enum FilterId {
    STATUS = "STATUS",
    ID = "ID",
    NAME = "NAME",
    DOCKER_NAME = "DOCKER_NAME",
    PROBLEM_ID = "PROBLEM_ID"
}

export enum FilterType {
    CHECK = "CHECK",
    RANGE = "RANGE",
    TEXT = "TEXT"
}

export type FilterModel =
    BaseFilterModel |
    CheckFilterModel |
    TextFilterModel;

export interface BaseFilterModel {
    id: FilterId;
    type: FilterType;
    name: string;
}

export interface CheckFilterModel extends BaseFilterModel {
    type: FilterType.CHECK;
    filterItems: {
        name: string;
        title?: string | ReactNode
    }[];
}

export interface TextFilterModel extends BaseFilterModel {
    type: FilterType.TEXT;
}

export interface FilterValue {
    id: FilterId;
    values: string[];
}
