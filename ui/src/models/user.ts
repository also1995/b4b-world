export interface User {
    id: string;
    email: string;
    role: string;
    entity: {
        id: string;
        name: string;
    }
}


export enum EUserRole {
    BUSINESS = "BUSINESS",
    BLOGGER = "BLOGGER"
}

export interface IBusinessUserRegister {
    name: string;
    site: string;
    email: string;
    password: string;
}

export interface IBloggerUserRegister {
    email: string;
    password: string;
    link: string;
    name: string;
    socialMedia: string;
}
