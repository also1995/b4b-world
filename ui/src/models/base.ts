import {FilterValue} from "./filter";

export interface DataResponse<T> {
    data: T[];
    total: number;
}

export interface FilteredDataRequestBody {
    filters: FilterValue[];
    page: number;
    count: number;
}