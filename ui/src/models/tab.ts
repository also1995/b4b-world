import React from "react";

export interface TabModel {
    name: string;
    key: string;
    image?: React.ReactNode;
}