const CracoLessPlugin = require('craco-less');
const CracoEnvPlugin = require('craco-plugin-env')

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            javascriptEnabled: true,
            modifyVars: { '@primary-color': '#8E4FFF',
              '@layout-header-background': '#8E4FFF',
              '@purple-base': '#8E4FFF',
              '@cyan-base': '#00CFD9',
              '@blue-base': '#0085FF'
            }
          },
        },
      },
    },
    {
      plugin: CracoEnvPlugin,
      options: {
        variables: {}
      }
    }
  ],
};
