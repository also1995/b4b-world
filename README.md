Python Application for verification energy (green-recs-verifier)

Comands:

* ``python -m venv venv `` - to create venv
* ``source venv/bin/activate `` - to activate venv
* ``python -m pip install -r requirements.txt`` - to install requirements
* ``pip freeze > requirements.txt  `` - run after adding dependency


For launch application: 

``python run.py``